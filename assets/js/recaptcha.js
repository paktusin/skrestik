const onSubmit = (el, key) => {
    const form = $(el).parents('form');
    form.append(`<input type="hidden" name="g-recaptcha-response" value="${key}"/>`);
    form.submit();
};

window.initRecaptcha = () => {
    $('form.recaptcha input[type="submit"]').each((key, el) => {
        grecaptcha.render(el, {
            'sitekey': $('#recaptcha').val(),
            'callback': (key) => {
                onSubmit(el, key)
            }
        })
    });
};