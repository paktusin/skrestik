const mulineList = [
    {name:'11',coef:0.0025249},
    {name:'14',coef:0.00196},
    {name:'16',coef:0.0016942},
    {name:'18',coef:0.0014689},
    {name:'20',coef:0.001272},
    {name:'22',coef:0.0010958},
];

export default mulineList;