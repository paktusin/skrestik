import React from 'react';
import mulineList from "./mulineList";
import Select from "./Select";

class Muline extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            muline: mulineList,
            selMuline: null,
            cross: 0,
            count: 1,
        }
    }

    changeCross(cross) {
        this.setState({...this.state, cross})
    }

    selectMuline(selMuline) {
        this.setState({...this.state, selMuline})
    }

    changeCount(count) {
        this.setState({...this.state, count})
    }

    render() {
        const answer = this.state.selMuline ? (this.state.selMuline.coef * this.state.cross * this.state.count).toFixed(1) : 0;
        return (
            <div className="muline">
                <div className="row">
                    <div className="col-12 col-md-4">
                        <div className="form-group">
                            <label>Кол-во крестиков:</label>
                            <input type="number"
                                   min={0}
                                   value={this.state.cross}
                                   onChange={e => this.changeCross(e.target.value)}
                                   className="form-control"/>
                        </div>
                    </div>
                    <div className="col-12 col-md-4">
                        <div className="form-group">
                            <label>Номер канвы Aida:</label>
                            <Select
                                value={this.state.selMuline}
                                onChange={this.selectMuline.bind(this)}
                                options={this.state.muline}
                                getOptionValue={muline => muline.name}
                                placeholder={"Выберите номер канвы"}
                                getOptionLabel={muline => muline.name}
                            >
                            </Select>
                        </div>
                    </div>
                    <div className="col-12 col-md-4">
                        <div className="form-group">
                            <label>Сложений нити:</label>
                            <input type="number"
                                   min={1}
                                   max={6}
                                   value={this.state.count}
                                   onChange={e => this.changeCount(e.target.value)}
                                   className="form-control"/>
                        </div>
                    </div>
                </div>
                {
                    this.state.selMuline && this.state.cross >= 50 &&
                    <div className="row">
                        <div className="col-12 mt-3">
                            <div className="alert alert-info">
                                На {this.state.selMuline.name} канве в {this.state.counts} сложение для
                                вышивки <b>{this.state.cross} крестиков</b> Вам
                                потребуется
                                ниток: <b>{answer} метров</b> или <b>{Math.ceil(answer / 8)} мотков</b>.
                            </div>
                        </div>
                    </div>
                }
            </div>
        )
            ;
    }
}

export default Muline;