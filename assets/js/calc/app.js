import ReactDOM from 'react-dom';
import React from 'react';
import Calc from './Calc';
import Cross from './Cross';
import Muline from "./Muline";

$(() => {
    ReactDOM.render(<Calc/>,document.getElementById('calc'));
    ReactDOM.render(<Cross/>,document.getElementById('cross'));
    ReactDOM.render(<Muline/>,document.getElementById('muline'));
});