import React from 'react';
import axios from 'axios';
import Select from './Select'
import './calc.scss';

class Calc extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            bases: [],
            selectedBase: null,
            delim: 1,
            size: {
                xCross: 0,
                yCross: 0,
                x: 0,
                y: 0
            }
        }
    }

    componentDidMount() {
        axios.get('/api/base', {params: {calc: true}}).then(res => {
            const bases = res.data;
            this.setState({
                ...this.state, bases: bases.reduce((prev, base) => {
                    let group = prev.findIndex(g => g.label === base.type);
                    if (group !== -1) {
                        prev[group].options.push(base);
                    } else {
                        prev.push({label: base.type, options: [base]});
                    }
                    return prev;
                }, [])
            });
        })
    }

    changeCross(val, type) {
        const size = {...this.state.size};
        size[type + 'Cross'] = val;
        size[type] = (val * this.state.selectedBase.coef * this.state.delim).toFixed(1);
        this.setState({...this.state, size});
    }

    change(val, type) {
        const size = {...this.state.size};
        size[type] = val;
        size[type + 'Cross'] = (val / this.state.selectedBase.coef / this.state.delim).toFixed(0);
        this.setState({...this.state, size});
    }

    select(base) {
        this.setState({...this.state, selectedBase: base}, () => {
            this.refreshSize()
        });
    }

    refreshSize() {
        const size = {...this.state.size};
        ['x', 'y'].forEach(type => {
            //size[type + 'Cross'] = (size[type] / this.state.selectedBase.coef / this.state.delim).toFixed(0);
            size[type] = (size[type] * this.state.selectedBase.coef * this.state.delim).toFixed(1);
        });
        this.setState({...this.state, size});
    }

    changeDelim(delim) {
        this.setState({...this.state, delim}, () => {
            this.refreshSize();
        })
    }

    render() {
        const bases = this.state.bases;
        const base = this.state.selectedBase;
        const size = this.state.size;
        return (
            <div className="canvas">
                <div className="row">
                    <div className="col-12 col-lg-4 offset-lg-4 mb-2">
                        <Select
                            value={base}
                            onChange={this.select.bind(this)}
                            options={bases}
                            getOptionValue={base => base.id}
                            placeholder={"Выберите основу"}
                            getOptionLabel={base => base.name}
                        >
                        </Select>
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-lg-4 offset-lg-4 mb-2">
                        <div className="form-row justify-content-center">
                            <div className="col flex-grow-0">
                                <label>Сантиметры:</label>
                                <input onChange={(e) => this.change(e.target.value, 'x')}
                                       type="number"
                                       disabled={base === null}
                                       className="form-control"
                                       value={size.x}/>
                            </div>
                            <div className="col flex-grow-0">
                                <label>Крестики:</label>
                                <input className="form-control"
                                       type="number"
                                       disabled={base === null}
                                       onChange={(e) => this.changeCross(e.target.value, 'x')}
                                       value={size.xCross}/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-lg-4 offset-lg-4 text-center">
                        <img className="img-fluid border shadow-lg" src={"/canvas.jpg"}/>
                    </div>
                    <div className="col-6 col-lg-4 d-flex flex-row justify-content-start">
                        <div className="d-flex justify-content-center flex-column">
                            <div className="form-group">
                                <label>Сантиметры:</label>
                                <input className="form-control"
                                       type="number"
                                       disabled={base === null}
                                       onChange={(e) => this.change(e.target.value, 'y')}
                                       value={size.y}/>
                            </div>
                            <div className="form-group">
                                <label>Крестики:</label>
                                <input className="form-control"
                                       type="number"
                                       onChange={(e) => this.changeCross(e.target.value, 'y')}
                                       disabled={base === null}
                                       value={size.yCross}/>
                            </div>
                            <div className="form-group">
                                <label>Через сколько нитей ткани вышивается крестик:</label>
                                <input className="form-control"
                                       type="number"
                                       min={1}
                                       onChange={(e) => this.changeDelim(e.target.value)}
                                       value={this.state.delim}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Calc;