const crosses = [
    {name:'Полукрестики',coef:0.5},
    {name:'Петиты',coef:0.5},
    {name:'Французские узелки',coef:1},
    {name:'Стежки бекстича',coef:0.5},
    {name:'Бисер',coef:0.5},
    {name:'1/4 крестики',coef:0.5},
    {name:'3/4 крестики',coef:1},
    {name:'Ковровая техника',coef:1},
    {name:'Крестики',coef:1}
];

export default crosses;