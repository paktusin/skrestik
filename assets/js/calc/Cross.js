import React from 'react';
import crosses from "./crosses";

class Cross extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            crosses
        }
    }

    change(val, index) {
        const crosses = [...this.state.crosses];
        crosses[index].value = val;
        this.setState({...this.state, crosses})
    }

    render() {
        const answer = this.state.crosses.reduce((prev, cross) => prev + Math.round(cross.coef * (cross.value | 0)), 0);
        return (
            <div className="cross">
                <div className="row">
                    {this.state.crosses.map((cross, key) =>
                        <div className="col-6 col-sm-4 col-md-3 col-lg-2" key={key}>
                            <div className="form-group">
                                <label>{cross.name}</label>
                                <input type="number"
                                       value={cross.value | 0}
                                       onChange={e => this.change(e.target.value, key)}
                                       className="form-control"/>
                            </div>
                        </div>
                    )}
                </div>
                <div className="row">
                    <div className="col-12 mt-3">
                        <div className="alert alert-info">
                            Введенное количество стежков аналогично <b>{answer}</b> крестикам.
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Cross;