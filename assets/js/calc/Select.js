import './Select.scss'
import React from "react";
import ReactSelect from "react-select";

const Select = (props) => <ReactSelect {...props} classNamePrefix={"select"}/>;

export default Select;