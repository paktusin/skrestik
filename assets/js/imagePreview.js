$(() => {
    $('[data-preview]').each((key, el) => {
        $(el).click(() => {
            imagePreview(el);
        })
    });
});

const imagePreview = (el) => {
    const url = $(el).find('img').first().attr('src');
    const template = `
    <div class="image-preview d-flex flex-column justify-content-center">
    <div class="d-block text-center">
    <img class="" src="${url}">
</div>
<div class="cls" title="Закрыть">
    <i class="fa fa-3x fa-times-circle"></i>
    </div>
</div>
    `;
    const imagePreview = $(template);
    imagePreview.click((e) => {
        if (e.target.tagName !== 'IMG') {
            $(document.body).removeClass('preview');
            imagePreview.animate({opacity: 0}, () => {
                imagePreview.remove();
            });
        }
    });
    $('body').append(imagePreview);
    const img = imagePreview.find('img');
    if (img.innerWidth() > img.innerHeight()) {
        img.addClass('hor');
    }
    $(document.body).addClass('preview');
    imagePreview.animate({opacity: 1});
};