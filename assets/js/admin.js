$(() => {
    $('.cke').each((key, el) => {
        CKEDITOR.replace($(el).attr('id'), {
            toolbar: [
                ['Source'],
                ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'],
                ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat'],
                ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                "/",
                ['Format', 'FontSize'], ['TextColor', 'BGColor'],
                ['Link', 'Unlink', 'Table', 'SpecialChar','ShowBlocks','CreateDiv']
            ],
            language: 'ru',
            height: 700,
            contentsCss: '/main_css'
        });
    });

});