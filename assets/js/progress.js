window.editProgress = (progress_id, edit = true) => {
    const modal = $('#editProgress');
    modal.find('.modal-body').html('');
    modal.modal('show');
    $.get(`/progress/${edit ? 'edit' : 'new'}/` + progress_id).then(res => {
        modal.find('.modal-body').html(res);
        initDatePickers();
    });
};