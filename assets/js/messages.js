$(() => {
    const messageTable = $('#message-table');
    getMessages({
        game_id: messageTable.data('game'),
        project_id: messageTable.data('project'),
        user_id: messageTable.data('user')
    }, 1).then(_ => {
        scrollMessages();
    });

    messageTable.parent().scroll((e) => {
        if (messageTable.parent().scrollTop() === 0) {
            const more = $('.more');
            if (more.length > 0) {
                const oldHeight = messageTable.innerHeight();
                getMessages({
                    game_id: messageTable.data('game'),
                    project_id: messageTable.data('project'),
                    user_id: messageTable.data('user')
                }, more.first().data('page')).then(res => {
                    messageTable.parent().scrollTop(messageTable.innerHeight() - oldHeight);
                    more.remove();
                });
            }
        }
    });
});

async function getMessages({game_id, project_id, user_id}, page = 1) {
    const url = user_id ? `/message/dialog/${user_id}/table` : '/comment';
    const html = await $.get(url, {game_id, project_id, page});
    $(html).insertAfter($('#message-table').find('.bg-message'));
    parseEmo();
    return true;
};