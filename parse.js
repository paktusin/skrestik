JSON.stringify([].slice.call($('#f_diz').children).reduce((prev, el, index) => {
    if (el.tagName === 'OPTION') prev.push({id: el.value, name: el.text.trim()});
    if (el.tagName === 'OPTGROUP') prev = prev.concat([].slice.call(el.children).map(sel => ({
        id: sel.value,
        name: sel.text.replace('|- ', ''),
        type: el.getAttribute('label').trim()
    })));
    return prev;
}, []));