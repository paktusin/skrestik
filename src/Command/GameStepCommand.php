<?php

namespace App\Command;

use App\Service\GameService;
use App\Service\ParseService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Constraints\Date;

class GameStepCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('game:step');

        $this->addArgument('date', InputArgument::OPTIONAL);
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $date = date_create_from_format('Y-m-d', $input->getArgument('date'));
        $str = ($date ? $date : new \DateTime())->format("d.m.Y 23:59:59");
        $output->writeln("step for {$str}");
        $this->getContainer()->get(GameService::class)->makeStep(($date ? $date : null));
    }
}
