<?php

namespace App\Controller;

use App\Entity\PlayerStep;
use App\Form\PlayerStepType;
use App\Service\PlayerStepService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/player_step")
 * @IsGranted("ROLE_USER")
 */
class PlayerStepController extends Controller
{
    /**
     * @Route("/edit/{id}")
     */
    public function edit($id, Request $request)
    {
        /** @var PlayerStep $playerStep */
        $playerStep = $this->get(PlayerStepService::class)->find($id);
        $this->checkAccess($playerStep);
        $form = $this->createForm(PlayerStepType::class, $playerStep);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($playerStep);
            $em->flush();
            return $this->redirectToRoute('app_game_steps', ['id' => $playerStep->getGameStep()->getGame()->getId()]);
        }
        return $this->render('player_step/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    private function checkAccess(PlayerStep $playerStep)
    {
        if (!($playerStep->getGameStep()->getGame()->getOrganizer() === $this->getUser() || $this->getUser()->hasRole('ROLE_SUPER_ADMIN')))
            throw $this->createAccessDeniedException('Вы не организатор игры');
    }
}
