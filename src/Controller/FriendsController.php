<?php

namespace App\Controller;

use App\Entity\FriendRequest;
use App\Entity\User;
use App\Service\UserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/friends")
 * @IsGranted("ROLE_USER")
 */
class FriendsController extends Controller
{
    /**
     * @Route("/")
     */
    public function index()
    {
        /** @var User $user */
        $user = $this->getUser();
        return $this->render('friends/index.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * @Route("/add/{id}")
     */
    public function addRequest($id, Request $request)
    {

        /** @var User $user */
        $user = $this->getUser();
        /** @var User|null $friend */
        $friend = $this->get(UserService::class)->find($id);
        $friendRequest = new FriendRequest();
        $friendRequest->setFrom($user);
        $friendRequest->setTo($friend);
        $friendRequest->setComment($request->request->get('comment'));
        $em = $this->getDoctrine()->getManager();
        $em->persist($friendRequest);
        $em->flush();
        $this->addFlash('info', 'Запрос отправлен');
        return $this->redirectToRoute('app_profile_show', ['id' => $id]);
    }

    /**
     * @Route("/cancel/{id}")
     */
    public function cancelRequest($id)
    {
        $friendRequest = $this->getDoctrine()->getRepository(FriendRequest::class)->find($id);
        if (!$friendRequest) throw $this->createNotFoundException('Запрос не найден');
        $em = $this->getDoctrine()->getManager();
        $em->remove($friendRequest);
        $em->flush();
        $this->addFlash('info', 'Запрос отменен');
        return $this->redirectToRoute('app_friends_index');
    }

    /**
     * @Route("/commit/{id}")
     */
    public function commitRequest($id, Request $request)
    {
        $commit = !$request->query->has('reject');
        $friendRequest = $this->getDoctrine()->getRepository(FriendRequest::class)->find($id);
        if (!$friendRequest) throw $this->createNotFoundException('Запрос не найден');
        $user1 = $friendRequest->getFrom();
        $user2 = $friendRequest->getTo();
        if ($commit) {
            $user1->addFriend($user2);
            $user2->addFriend($user1);
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($friendRequest);
        $em->flush();
        $this->addFlash('info', 'Запрос ' . ($commit ? 'принят' : 'отклонен'));
        return $this->redirectToRoute('app_friends_index');
    }

    /**
     * @Route("/remove/{id}")
     */
    public function removeFriend($id, Request $request)
    {
        /** @var User $user1 */
        $user1 = $this->getUser();
        /** @var User $user2 */
        $user2 = $this->get(UserService::class)->find($id);
        $user1->removeFriend($user2);
        $user2->removeFriend($user1);
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        $this->addFlash('info', "Пользователь {$user2->getName()} удален из друзей");
        return $this->redirectToRoute('app_friends_index');
    }
}
