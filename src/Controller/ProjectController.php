<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Image;
use App\Entity\Player;
use App\Entity\Project;
use App\Entity\User;
use App\Form\ProjectType;
use App\Service\ImageService;
use App\Service\MessageService;
use App\Service\ProjectService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/project")
 */
class ProjectController extends Controller
{
    /**
     * @Route("/")
     */
    public function index(Request $request)
    {
        $search = $request->query->get('search');
        $query = $this->get(ProjectService::class)->query('p')
            ->innerJoin('p.user', 'user')
            ->andwhere('p.hidden != 1');
        if ($search) {
            $query->andwhere("p.name like '%{$search}%' OR user.username like '%{$search}%'");
        } else {
            $query
                ->innerJoin('p.image', 'image')
                ->andWhere('p.start is not null');
        }
        $query->orderBy('p.id', 'DESC');
        $projects = $query->getQuery();
        //$this->get(ProjectService::class)->repo()->findBy([], ['start' => 'DESC', 'id' => 'DESC']);
        return $this->render('project/index.html.twig', [
            'search' => $search,
            'projects' => $this->get('knp_paginator')->paginate($projects
                , $request->query->getInt('page', 1),
                24)
        ]);
    }

    /**
     * @Route("/{id}/show")
     */
    public function show($id)
    {
        /** @var Project $project */
        $project = $this->get(ProjectService::class)->find($id);
        $user = $this->getUser();
        if (!(!$project->getHidden() || $user && ($user == $project->getUser() || $user->hasRole('ROLE_SUPER_ADMIN')))) throw $this->createAccessDeniedException('Проект скрытый');
        return $this->render('project/show.html.twig', [
            'project' => $project
        ]);
    }

    /**
     * @Route("/add_watch/{id}")
     * @IsGranted("ROLE_USER")
     */
    public function addTowWatch($id)
    {
        /** @var Project $project */
        $project = $this->get(ProjectService::class)->find($id);
        /** @var User $user */
        $user = $this->getUser();
        if ($user == $project->getUser()) return $this->redirectToRoute('app_project_show', ['id' => $id]);
        $user->addWatchProject($project);
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        $this->addFlash('info', 'Проект добавлен в список отслеживаемых');
        return $this->redirectToRoute('app_project_show', ['id' => $id]);
    }

    /**
     * @Route("/add_photo/{id}",methods={"POST"})
     * @IsGranted("ROLE_USER")
     */
    public function addPhoto($id, Request $request)
    {
        /** @var Project $project */
        $project = $this->get(ProjectService::class)->find($id);
        $this->checkAccess($project);
        $image = $this->get(ImageService::class)->upload($request->files->get('image_file'), 800, 600, false, $request->request->getBoolean('mark', false));
        $project->addImage($image);
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        return $this->redirectToRoute('app_project_photos', ['id' => $id]);
    }

    public function checkAccess(Project $project)
    {
        /** @var User $user */
        $user = $this->getUser();
        if (!($user === $project->getUser() || $user->hasRole('ROLE_SUPER_ADMIN'))) throw $this->createNotFoundException('Вы не можете редактировать этот проект');
    }

    /**
     * @Route("/remove_photo/{id}")
     * @IsGranted("ROLE_USER")
     */
    public function removePhoto($id, Request $request)
    {
        /** @var Project $project */
        $project = $this->get(ProjectService::class)->find($id);
        /** @var Image $image */
        $image = $this->get(ImageService::class)->find($request->query->get('image_id'));
        $this->checkAccess($project);

        $em = $this->getDoctrine()->getManager();
        $project->removeImage($image);
        $em->remove($image);
        $em->flush();

        return $this->redirectToRoute('app_project_photos', ['id' => $id]);
    }

    /**
     * @Route("/remove_watch/{id}")
     * @IsGranted("ROLE_USER")
     */
    public function removeTowWatch($id)
    {
        /** @var Project $project */
        $project = $this->get(ProjectService::class)->find($id);
        /** @var User $user */
        $user = $this->getUser();
        $user->removeWatchProject($project);
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        $this->addFlash('info', 'Проект удален из списка отслеживаемых');
        return $this->redirectToRoute('app_project_show', [
            'id' => $id
        ]);
    }

    /**
     * @Route("/watch")
     * @IsGranted("ROLE_USER")
     */
    public function watchList()
    {
        return $this->render('profile/watch.html.twig');
    }

    /**
     * @Route("/{id}/comments")
     */
    public function comments($id)
    {
        /** @var Project $project */
        $project = $this->get(ProjectService::class)->find($id);
        return $this->render('project/comments.html.twig', [
            'project' => $project
        ]);
    }

    /**
     * @Route("/{id}/comments/read")
     */
    public function commentsRead($id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Project $project */
        $project = $this->get(ProjectService::class)->find($id);
        if ($project->getUser() === $this->getUser()) {
            /** @var Comment[] $newComments */
            $newComments = $this->getDoctrine()->getRepository(Comment::class)->findBy(['project' => $project, 'readed' => false]);
            foreach ($newComments as $newComment) {
                $newComment->setReaded(true);
            }
            $em->flush();
        }
        return $this->json('ok');
    }

    /**
     * @Route("/{id}/add_comment", methods={"POST"})
     * @IsGranted("ROLE_USER")
     */
    public function addComment($id, Request $request)
    {
        /** @var Project $project */
        $project = $this->get(ProjectService::class)->find($id);
        $commentText = trim($request->request->get('comment'));
        if (strlen($commentText) === 0) {
            $this->addFlash('info', 'Вы ничего не написали');
        } else {
            $comment = new Comment();
            $comment->setProject($project);
            $comment->setUser($this->getUser());
            $comment->setReaded($this->getUser() === $project->getUser());
            $comment->setComment($commentText);
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();
            $this->get(MessageService::class)->mailComment($this->getUser(), $project);
        }
        return $this->redirectToRoute('app_project_comments', ['id' => $id]);
    }

    /**
     * @Route("/{id}/photos")
     */
    public function photos($id)
    {
        /** @var Project $project */
        $project = $this->get(ProjectService::class)->find($id);
        return $this->render('project/photos.html.twig', [
            'project' => $project
        ]);
    }

    /**
     * @Route("/new")
     * @IsGranted("ROLE_USER")
     */
    public function create(Request $request)
    {
        $user = $this->getUser();
        /** @var Project $project */
        $project = new Project();
        $project->setUser($user);
        $form = $this->createForm(ProjectType::class, $project);
        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);
            $em = $this->getDoctrine()->getManager();
            $em->persist($project);
            $em->flush();
            $image_file = $request->files->get('project')['image_file'];
            if ($image_file) {
                $project->setImage($this->get(ImageService::class)->upload($image_file, 800, 600, false, $request->request->getBoolean('mark', false)));
            }
            $em->flush();
            $this->addFlash('info', 'Проект добавлен');
            return $this->redirectToRoute('app_profile_show', ['id' => $user->getId()]);
        }
        return $this->render('project/edit.html.twig', [
            'form' => $form->createView(),
            'project' => $project,
        ]);
    }

    /**
     * @Route("/edit/{id}")
     * @IsGranted("ROLE_USER")
     * @throws \Gumlet\ImageResizeException
     */
    public function edit(Request $request, $id)
    {
        /** @var Project $project */
        $project = $this->get(ProjectService::class)->find($id);
        $this->checkAccess($project);
        $form = $this->createForm(ProjectType::class, $project);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $image_file = $request->files->get('project')['image_file'];
            if ($image_file) {
                $project->setImage($this->get(ImageService::class)->upload($image_file, 800, 600, false, $request->request->getBoolean('mark', false)));
            }
            $em->persist($project);
            if ($project->getEnd() && $project->getActivePlayers()->count() > 0) {
                $end = $project->getEnd();
                $project->setEnd(null);
                $em->flush();
                return $this->redirectToRoute('app_project_endwithgame', ['id' => $id]);
            }
            $em->flush();
            $this->addFlash('info', 'Проект изменен');
            return $this->redirectToRoute('app_project_show', ['id' => $id]);
        }
        return $this->render('project/edit.html.twig', [
            'form' => $form->createView(),
            'project' => $project,
        ]);
    }

    /**
     * @Route("/end/{id}/with_game")
     * @IsGranted("ROLE_USER")
     */
    public function endWithGame(Request $request, $id)
    {

        /** @var Project $project */
        $project = $this->get(ProjectService::class)->find($id);
        $this->checkAccess($project);
        $currentProjects = $this->container->get(ProjectService::class)->getCurrent($this->getUser(), true);
        $form = $this->createFormBuilder()
            ->add('project', EntityType::class, [
                'class' => Project::class,
                'choices' => $currentProjects
            ])
            ->add('change', HiddenType::class, [
                'data' => 1
            ])
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            /** @var Player $player */
            foreach ($project->getActivePlayers() as $player) {
                $lastProjects = $player->getLastProjects();
                array_push($lastProjects, [
                    'id' => $player->getProject()->getId(),
                    'date' => (new \DateTime())->format('Y-m-d H:i:s'),
                    'done' => true,
                ]);
                $player->setLastProjects($lastProjects);
            }

            if ($form->get('change')->getData()) {
                $player->setProject($form->get('project')->getData());
                $this->addFlash('info', 'Вы заменили проект в игре');
            } else {
                foreach ($project->getActivePlayers() as $player) {
                    $player->setGameOver(true);
                }
                $this->addFlash('info', 'Вы покинули игру');
            }
            $project->setEnd(new \DateTime());
            $em->flush();
            return $this->redirectToRoute('app_project_show', ['id' => $id]);
        }
        return $this->render('project/end.html.twig', [
            'form' => $form->createView(),
            'projects' => $currentProjects
        ]);
    }

    /**
     * @Route("/remove/{id}")
     * @IsGranted("ROLE_USER")
     */
    public function remove(Request $request, $id)
    {
        /** @var Project $project */
        $project = $this->get(ProjectService::class)->find($id);
        $user = $project->getUser();
        $this->checkAccess($project);
        $em = $this->getDoctrine()->getManager();
        $em->remove($project);
        $em->flush();
        $this->addFlash('info', 'Проект удален');
        return $this->redirectToRoute('app_profile_show', ['id' => $user->getId()]);
    }

//    /**
//     * @Route("/finish/{id}")
//     * @IsGranted("ROLE_USER")
//     */
//    public function finish($id, Request $request)
//    {
//        /** @var Project $project */
//        $project = $this->get(ProjectService::class)->find($id);
//        $this->checkAccess($project);
//        $again = $request->query->has('again');
//        $project->setEnd($again ? null : new \DateTime());
//        $this->getDoctrine()->getManager()->flush();
//        $this->addFlash('success', 'Проект ' . ($again ? 'перенесен в текущие' : 'завершен'));
//        return $this->redirectToRoute('fos_user_profile_show', ['id' => $project->getUser()->getId()]);
//    }
}
