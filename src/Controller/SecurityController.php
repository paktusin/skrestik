<?php
/**
 * Created by PhpStorm.
 * User: paktus-lnx
 * Date: 06.12.17
 * Time: 22:02
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends Controller
{
    /**
     * @Route("/login/")
     */
    public function loginAction(Request $request)
    {
        if ($request->getMethod() === 'GET') return $this->get('fos_user.security.controller')->loginAction($request);
        if ($this->checkCaptcha($request->request->get('g-recaptcha-response'))) {
            return $this->get('fos_user.security.controller')->loginAction($request);
        } else {
            $this->addFlash('danger', 'Вы не прошли проверку на робота');
            return $this->redirectToRoute('fos_user_security_login');
        }
    }

    /**
     * @Route("/register/")
     */
    public function registerAction(Request $request)
    {
        if ($request->getMethod() === 'GET') return $this->get('fos_user.registration.controller')->registerAction($request);
        if ($this->checkCaptcha($request->request->get('g-recaptcha-response'))) {
            return $this->get('fos_user.registration.controller')->registerAction($request);
        } else {
            $this->addFlash('danger', 'Вы не прошли проверку на робота');
            return $this->redirectToRoute('fos_user_registration_register');
        }
    }

    /**
     * @Route("/send-email/")
     */
    public function sendEmailAction(Request $request)
    {
        if ($this->checkCaptcha($request->request->get('g-recaptcha-response'))) {
            return $this->get('fos_user.resetting.controller')->sendEmailAction($request);
        } else {
            $this->addFlash('danger', 'Вы не прошли проверку на робота');
            return $this->redirectToRoute('fos_user_resetting_request');
        }
    }

    public function checkCaptcha($captcha)
    {
//        $proxy = $this->container->getParameter('proxy');
        $need_captcha = strlen($this->container->getParameter('recaptcha_secret')) > 0;
        if ($need_captcha) {
            $key = $this->container->getParameter('recaptcha_secret');
            $url = "https://www.google.com/recaptcha/api/siteverify?secret={$key}&response=" . $captcha;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            //curl_setopt($ch, CURLOPT_VERBOSE, true);
//            if ($proxy !== '') curl_setopt($ch, CURLOPT_PROXY, $proxy);
            $response = curl_exec($ch);
            curl_close($ch);
            $obj = json_decode($response);
        }
        return !$need_captcha || $obj->success == true;
    }


}