<?php

namespace App\Controller;

use App\Entity\Base;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/api")
 */
class ApiController extends Controller
{
    /**
     * @Route("/base")
     */
    public function base(Request $request)
    {
        $query = $this->getDoctrine()->getRepository(Base::class)
            ->createQueryBuilder('b')
            ->orderBy('b.name')
            ->orderBy('b.count')
        ;
        if ($request->query->has('calc')) {
            $query->andWhere('b.coef > 0');
        }
        return $this->json($query->getQuery()->getResult());
    }
}
