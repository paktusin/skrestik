<?php

namespace App\Controller;

use App\Entity\Project;
use App\Service\GameService;
use App\Service\PageService;
use App\Service\ProjectService;
use App\Service\StepService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function index(Request $request)
    {
        return $this->page('home');
    }

    /**
     * @Route("/calc")
     */
    public function calc()
    {
        return $this->render('calc.html.twig');
    }

//    /**
//     * @Route("/test")
//     */
//    public function test(Request $request)
//    {
//        $this->get(GameService::class)->makeStep(date_create_from_format('Y-m-d', '2019-02-04'));
//        return $this->json([]);
//    }

    /**
     * @Route("/page/{url}")
     */
    public function page($url)
    {
        return $this->render('page.html.twig', [
            'page' => $this->get(PageService::class)->find($url)
        ]);
    }

    /**
     * @Route("/sitemap.xml", defaults={"_format"="xml"}, name="sitemap")
     */
    public function siteMapAction()
    {
        $lines = array_map(function (Project $project) {
            return $this->generateUrl('app_project_show', ['id' => $project->getId()], 0);
        }, $this->get(ProjectService::class)->repo()->findBy(['hidden' => false]));
        return $this->render('sitemap.xml.twig', [
            'lines' => $lines
        ]);
    }

    /**
     * @Route("/main_css")
     */
    public function mainCss()
    {
        $manifest = json_decode(file_get_contents($this->get('kernel')->getRootDir() . '/../public/build/manifest.json'), true);
        $css = file_get_contents($this->get('kernel')->getRootDir() . '/../public' . $manifest['build/app.css']);
        return new Response($css, 200, ['Content-Type' => 'text/css']);
    }

}
