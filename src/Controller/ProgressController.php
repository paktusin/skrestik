<?php

namespace App\Controller;

use App\Entity\FriendRequest;
use App\Entity\Progress;
use App\Entity\Project;
use App\Entity\User;
use App\Form\ProgressType;
use App\Service\ProgressService;
use App\Service\ProjectService;
use App\Service\UserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/progress")
 * @IsGranted("ROLE_USER")
 */
class ProgressController extends Controller
{
    /**
     * @Route("/new/{project_id}")
     */
    public function create($project_id, Request $request)
    {
        /** @var Project $project */
        $project = $this->get(ProjectService::class)->find($project_id);
        $this->checkAccess($project);
        $progress = new Progress();
        $progress->setProject($project);
        $form = $this->createForm(ProgressType::class, $progress);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($progress);
            $em->flush();
            return $this->redirectToRoute('app_project_show', ['id' => $project_id]);
        }
        return $this->render('progress/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}")
     */
    public function edit($id, Request $request)
    {
        /** @var Progress $progress */
        $progress = $this->get(ProgressService::class)->find($id);
        $this->checkAccess($progress->getProject());
        $form = $this->createForm(ProgressType::class, $progress);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($progress);
            $em->flush();
            return $this->redirectToRoute('app_project_show', ['id' => $progress->getProject()->getId()]);
        }
        return $this->render('progress/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function checkAccess(Project $project)
    {
        /** @var User $user */
        $user = $this->getUser();
        if (!($user === $project->getUser() || $user->hasRole('ROLE_SUPER_ADMIN'))) throw $this->createNotFoundException('Вы не можете редактировать этот проект');
    }

    /**
     * @Route("/remove/{id}")
     */
    public function remove($id)
    {
        /** @var Progress $user */
        $progress = $this->get(ProgressService::class)->find($id);
        $project_id = $progress->getProject()->getId();
        $this->checkAccess($progress->getProject());
        $em = $this->getDoctrine()->getManager();
        $em->remove($progress);
        $em->flush();
        return $this->redirectToRoute('app_project_show', ['id' => $project_id]);
    }
}
