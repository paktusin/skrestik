<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Game;
use App\Entity\Project;
use App\Service\CommentService;
use App\Service\GameService;
use App\Service\ProjectService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/comment")
 */
class CommentController extends Controller
{
    /**
     * @Route("/edit/{comment_id}")
     * @param $comment_id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @IsGranted("ROLE_USER")
     */
    public function editComment($comment_id, Request $request)
    {
        $game = null;
        $project = null;
        if ($request->query->has('game_id')) {
            $game = $this->get(GameService::class)->repo()->find($request->query->get('game_id'));
        }
        if ($request->query->has('project_id')) {
            /** @var Project $project */
            $project = $this->get(ProjectService::class)->repo()->find($request->query->get('project_id'));
        }
        if ($project || $game) {
            /** @var Comment $comment */
            $comment = $this->get(CommentService::class)->find($comment_id);
            $this->get(CommentService::class)->editComment($request, $comment, $this->getUser(), $game, $project);
        }
        if ($game) return $this->redirectToRoute('app_game_comments', ['id' => $game->getId()]);
        if ($project) return $this->redirectToRoute('app_project_comments', ['id' => $project->getId()]);
        return $this->redirectToRoute('fos_user_profile_show');
    }


    /**
     * @Route("/")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getComments(Request $request)
    {
        $limit = 20;
        $page = $request->query->getInt('page', 1);
        /** @var Game $game */
        $game = $this->get(GameService::class)->repo()->find($request->query->getInt('game_id', 0));
        $project = $this->get(ProjectService::class)->repo()->find($request->query->getInt('project_id', 0));
        $q = $this->get(CommentService::class)->query('c')->orderBy('c.created_at', 'DESC');
        if ($game) {
            $q->join('c.game', 'game')
                ->andWhere("game.id = {$game->getId()}");
        }
        if ($project) {
            $q->join('c.project', 'project')
                ->andWhere("project.id = {$project->getId()}");
        }
        if ($game || $project) {
            $comments = $q->getQuery();
        } else {
            $comments = [];
        }
        return $this->render('comments_table.html.twig', [
            'comments' => $this->get('knp_paginator')->paginate($comments, $page, $limit),
            'game' => $game,
            'project' => $project,
        ]);
    }
}
