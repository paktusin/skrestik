<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\User;
use App\Form\ProfileType;
use App\Service\GameService;
use App\Service\ImageService;
use App\Service\ProjectService;
use App\Service\UserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/profile")
 */
class ProfileController extends Controller
{
    /**
     * @Route("/", name="fos_user_profile_show")
     */
    public function index()
    {
        if (!$this->getUser()) return $this->redirectToRoute('fos_user_registration_register');
        return $this->profilePageShow($this->getUser());
    }

    /**
     * @Route("/{id}/show")
     */
    public function show($id)
    {
        /** @var User $user */
        $user = $this->get(UserService::class)->find($id);
        return $this->profilePageShow($user);
    }

    /**
     * @Route("/{id}/gallery")
     */
    public function gallery($id)
    {
        /** @var User $user */
        $user = $this->get(UserService::class)->find($id);
        $projects = $this->get(ProjectService::class)->query('p')
            ->innerJoin('p.user', 'user')
            ->andWhere("user.id = {$user->getId()} ")
            ->andWhere('p.end is not null')
            ->andWhere('p.image is not null')
            ->orderBy('p.end', 'DESC')
            ->getQuery()
            ->getResult();
        return $this->render('/profile/gallery.html.twig', [
            'projects' => $projects,
            'user' => $user
        ]);
    }

    /**
     * @Route("/{id}/games")
     */
    public function games($id)
    {
        /** @var User $user */
        $user = $this->get(UserService::class)->find($id);
        return $this->render('/profile/games.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * @Route("/edit")
     * @IsGranted("ROLE_USER")
     * @throws \Gumlet\ImageResizeException
     */
    public function edit(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($request->query->has('user_id') && $this->getUser()->hasRole('ROLE_SUPER_ADMIN')) {
            $user = $this->get(UserService::class)->find($request->query->get('user_id'));
            if ($request->request->has('new-password')) {
                $user->setPlainPassword($request->request->get('new-password'));
                $user->setEnabled($request->request->getBoolean('enabled'));
            }
        }
        $form = $this->createForm(ProfileType::class, $user);
        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);
            $em = $this->getDoctrine()->getManager();
            $image_file = $request->files->get('profile')['image_file'];
            if ($image_file) {
                $user->setImage($this->get(ImageService::class)->upload($image_file, 100, 100, true, false));
            }
            $em->flush();
            $this->addFlash('info', 'Профиль изменен');
            return $this->redirectToRoute('app_profile_show', ['id' => $user->getId()]);
        }
        return $this->render('profile/edit.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
        ]);
    }

    private function profilePageShow(User $user)
    {
        return $this->render('profile/index.html.twig', [
            'user' => $user,
            'current_projects' => $this->get(ProjectService::class)->getCurrent($user),
            'future_projects' => $this->get(ProjectService::class)->getFuture($user),
            'done_projects' => $this->get(ProjectService::class)->getDone($user)
        ]);
    }
}
