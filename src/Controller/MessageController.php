<?php

namespace App\Controller;

use App\Entity\Message;
use App\Entity\User;
use App\Service\MessageService;
use App\Service\UserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/message")
 * @IsGranted("ROLE_USER")
 */
class MessageController extends Controller
{

    /**
     * @Route("/")
     */
    public function index()
    {
        return $this->render('message/index.html.twig', [
            'userLastMessage' => $this->get(UserService::class)->dialogUsers($this->getUser()),
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/read/{id}")
     */
    public function read($id)
    {
        /** @var User $toUser */
        $toUser = $this->get(UserService::class)->find($id);
        $messages = $this->get(MessageService::class)->getDialog($this->getUser(), $toUser)->getResult();
        $em = $this->getDoctrine()->getManager();
        foreach ($messages as $message) {
            if ($message->getTo() === $this->getUser()) $message->setReaded(true);
        }
        $em->flush();
        return $this->json('ok');
    }

    /**
     * @Route("/dialog/{id}")
     */
    public function dialog($id)
    {
        /** @var User $toUser */
        $toUser = $this->get(UserService::class)->find($id);
        if ($this->getUser() === $toUser) return $this->redirectToRoute('app_message_index');
        return $this->render('message/dialog.html.twig', [
            'user' => $toUser
        ]);
    }

    /**
     * @Route("/dialog/{id}/table")
     */
    public function dialogTable($id, Request $request)
    {
        /** @var User $toUser */
        $toUser = $this->get(UserService::class)->find($id);
        if ($this->getUser() === $toUser) return $this->redirectToRoute('app_message_index');
        return $this->render('message/dialog_table.html.twig', [
            'messages' => $this->get('knp_paginator')->paginate($this->get(MessageService::class)->getDialog($this->getUser(), $toUser), $request->query->getInt('page', 1), 20),
            'user' => $toUser
        ]);
    }

    /**
     * @Route("send/{id}", methods={"POST"})
     */
    public function sendMessage($id, Request $request)
    {
        /** @var User $toUser */
        $toUser = $this->get(UserService::class)->find($id);
        if ($this->getUser() === $toUser) $this->redirectToRoute('app_message_index');
        $messageText = trim($request->request->get('message'));
        if (strlen($messageText) === 0) {
            $this->addFlash('info', 'Вы ничего не написали');
        } else {
            $message = new Message();
            $message->setFrom($this->getUser());
            $message->setTo($toUser);
            $message->setMessage($messageText);
            $em = $this->getDoctrine()->getManager();
            $em->persist($message);
            $em->flush();
            $this->get(MessageService::class)->mailMessage($this->getUser(), $toUser);
        }
        return $this->redirectToRoute('app_message_dialog', ['id' => $id]);
    }
}
