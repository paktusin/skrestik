<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Game;
use App\Entity\GameStep;
use App\Entity\Player;
use App\Entity\Project;
use App\Entity\Reward;
use App\Entity\User;
use App\Form\GameType;
use App\Service\CommentService;
use App\Service\CommonService;
use App\Service\GameService;
use App\Service\PlayerService;
use App\Service\PlayerStepService;
use App\Service\ProjectService;
use App\Service\RewardTypeService;
use App\Service\StepService;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/game")
 */
class GameController extends Controller
{
    /**
     * @Route("/")
     */
    public function index()
    {
        return $this->render('/game/index.html.twig', [
            'games' => $this->get(GameService::class)->getGames(true, true)
        ]);
    }

    /**
     * @Route("/archive")
     */
    public function archive()
    {
        return $this->render('/game/archive.html.twig', [
            'games' => $this->get(GameService::class)->getGames(true, false)
        ]);
    }

    /**
     * @Route("/{id}/show")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show($id)
    {
        /** @var Game $game */
        $game = $this->get(GameService::class)->find($id);
        return $this->render('/game/show.html.twig', [
            'game' => $game
        ]);
    }

    /**
     * @Route("/{id}/steps")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function steps($id)
    {
        /** @var Game $game */
        $game = $this->get(GameService::class)->find($id);
        return $this->render('/game/steps.html.twig', [
            'game' => $game,
            'nextDate' => $this->get(CommonService::class)->nearDay($game->lastStepDate(), $game->getStepDay(), $game->lastStepDate() === $game->getStart()),
        ]);
    }

    /**
     * @Route("/{id}/comments")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function comments($id)
    {
        /** @var Game $game */
        $game = $this->get(GameService::class)->find($id);
        return $this->render('/game/comments.html.twig', [
            'game' => $game
        ]);
    }

    /**
     * @Route("/{id}/comments/add")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @IsGranted("ROLE_USER")
     */
    public function addComment($id, Request $request)
    {
        /** @var Game $game */
        $game = $this->get(GameService::class)->find($id);
        $this->get(CommentService::class)->addComment($request->request->get('comment'), $this->getUser(), $game);
        return $this->redirectToRoute('app_game_comments', ['id' => $game->getId()]);
    }

    /**
     * @Route("/{game_step_id}/steps/table")
     * @param $game_step_id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function stepsTable($game_step_id, Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        /** @var GameStep $gameStep */
        $gameStep = $this->get('doctrine')->getRepository(GameStep::class)->find($game_step_id);
        if (!$gameStep) throw  $this->createNotFoundException('Нет ходов за данный период');
        $players = $gameStep->getGame()->getPlayers()->toArray();

        usort($players, function (Player $player_a, Player $player_b) use ($gameStep) {
            $step_a = $player_a->getStepByStep($gameStep);
            $step_b = $player_b->getStepByStep($gameStep);
            if (is_null($step_a) && is_null($step_b)) {
                return strcmp($player_a->getUser()->getUsername(), $player_b->getUser()->getUsername());
            } else {
                if (is_null($step_b)) return -1;
                if (is_null($step_a)) return 1;
                if ($step_a->getCount() === $step_b->getCount()) {
                    return strcmp($player_a->getUser()->getUsername(), $player_b->getUser()->getUsername());
                } else {
                    return $step_a->getCount() > $step_b->getCount() ? -1 : 1;
                }
            }
        });
        return $this->render('/game/steps/table.html.twig', [
            'players' => $players,
            'game_step' => $gameStep,
            'can_edit' => $this->canEdit($gameStep->getGame(), $user)
        ]);
    }

    private function canEdit(Game $game, $user)
    {
        if (!$user) return false;
        return ($game->getOrganizer() === $user || $user && $user->hasRole('ROLE_SUPER_ADMIN'));
    }

    /**
     * @Route("/moderate")
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function moderate()
    {
        $games = $this->get(GameService::class)->getGames();
        return $this->render('/game/moderate.html.twig', [
            'games' => $games
        ]);
    }

    /**
     * @Route("/{id}/approve")
     * @IsGranted("ROLE_SUPER_ADMIN")
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function approve(Request $request, $id)
    {
        /** @var Game $game */
        $game = $this->get(GameService::class)->find($id);
        $approve = $request->query->has('approve');
        $game->setModerated($approve);
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        $this->addFlash($approve ? 'success' : 'danger', "Игра " . ($approve ? 'одобрена' : 'отклонена'));
        return $this->redirectToRoute('app_game_moderate');
    }

    /**
     * @Route("/new")
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function create(Request $request)
    {
        $user = $this->getUser();
        if (!$this->get(GameService::class)->canGame($user)) {
            $this->addFlash('info', 'Вы пока не можете создавать игры');
            return $this->redirectToRoute('app_profile_games', ['id' => $user->getId()]);
        }
        $game = new Game();
        $game->setOrganizer($this->getUser());
        $form = $this->createForm(GameType::class, $game);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->canCreate($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($game);
            $em->flush();
            $this->addFlash('info', 'Игра отправлена на модерацию, после одобрения ее смогут увидеть другие участники');
            return $this->redirectToRoute('app_profile_games', ['id' => $this->getUser()->getId()]);
        }
        return $this->render('/game/edit.html.twig', [
            'form' => $form->createView(),
            'game' => $game
        ]);
    }

    /**
     * @Route("/{id}/edit")
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(Request $request, $id)
    {
        /** @var Game $game */
        $game = $this->get(GameService::class)->find($id);
        $this->checkAccess($game);
        $form = $this->createForm(GameType::class, $game);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $this->addFlash('info', 'Игра изменена');
            return $this->redirectToRoute('app_profile_games', ['id' => $this->getUser()->getId()]);
        }
        return $this->render('/game/edit.html.twig', [
            'form' => $form->createView(),
            'game' => $game
        ]);
    }

    /**
     * @Route("/{id}/remove")
     * @IsGranted("ROLE_USER")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function remove($id)
    {
        /** @var Game $game */
        $game = $this->get(GameService::class)->find($id);
        $this->checkAccess($game);
        if ($game->getModerated()) throw $this->createAccessDeniedException('Игра не может быть удалена');
        $em = $this->getDoctrine()->getManager();
        $em->remove($game);
        $em->flush();
        $this->addFlash('info', 'Игра удалена');
        return $this->redirectToRoute('app_profile_games', ['id' => $this->getUser()->getId()]);
    }

    private function checkAccess(Game $game)
    {
        /** @var User $user */
        $user = $this->getUser();
        if (!($game->getOrganizer() === $user || $user->hasRole('ROLE_SUPER_ADMIN'))) throw $this->createAccessDeniedException('Вы не можете редактировать игру');
    }

    private function canCreate(?User $user)
    {
        if (!$this->get(GameService::class)->canGame($user)) throw $this->createAccessDeniedException('Вы не можете создавать игры');
    }

    /**
     * @Route("/{id}/join")
     * @IsGranted("ROLE_USER")
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function join($id, Request $request)
    {
        $currentProjects = $this->container->get(ProjectService::class)->getCurrent($this->getUser(), true);
        if (count($currentProjects) === 0) {
            $this->addFlash('info', 'Вы не можете присоединиться к игре, так как у вас нет текущих проектов');
            return $this->redirectToRoute('app_game_show', ['id' => $id]);
        }
        $game = $this->get(GameService::class)->find($id);

        if (!is_null($game->getPlayerForUser($this->getUser()))) {
            $this->addFlash('info', 'Вы уже в игре');
            return $this->redirectToRoute('app_game_show', ['id' => $id]);
        }
        $form = $this->createFormBuilder()
            ->add('project', EntityType::class, [
                'class' => Project::class,
                'choices' => $currentProjects
            ])->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            /** @var Project $project */
            $player = $this->get(PlayerService::class)->repo()->findOneBy(['game' => $game, 'user' => $user]);
            if (is_null($player)) $player = new Player();
            $player->setUser($user);
            $player->setGame($game);
            $player->setGameOver(false);
            $player->setProject($form->get('project')->getData());
            $em = $this->getDoctrine()->getManager();
            $em->persist($player);
            $em->flush();
            $this->addFlash('info', 'Вы присоединились к игре');
            return $this->redirectToRoute('app_game_show', ['id' => $id]);
        }
        return $this->render('/game/join.html.twig', [
            'game' => $game,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/leave")
     * @IsGranted("ROLE_USER")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function leave($id)
    {
        /** @var Game $game */
        $game = $this->get(GameService::class)->find($id);
        $player = $game->getPlayerForUser($this->getUser());
        $player->setGameOver(true);
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        $this->get(GameService::class)->quitGame($game, $player->getProject(), $player);
        $this->addFlash('info', 'Вы прекратили игру');
        return $this->redirectToRoute('app_game_show', ['id' => $id]);
    }

    /**
     * @Route("/{id}/dismiss")
     * @IsGranted("ROLE_USER")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function dismiss($id)
    {
        /** @var Player $player */
        $player = $this->container->get(PlayerService::class)->find($id);
        if (!$player) throw $this->createNotFoundException('Игрок не найден');
        $game = $player->getGame();
        if (!$this->canEdit($game, $this->getUser())) throw $this->createAccessDeniedException('Вы не можете исключать игроков');
        $player->setGameOver(true);
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        $this->get(GameService::class)->quitGame($game, $player->getProject(), $player);
        $this->addFlash('info', "Вы исключили игрока {$player->getUser()->getUsername()} из игры");
        return $this->redirectToRoute('app_game_steps', ['id' => $game->getId()]);
    }

    /**
     * @Route("/{id}/project_change")
     * @IsGranted("ROLE_USER")
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function projectChange($id, Request $request)
    {
        /** @var Game $game */
        $game = $this->get(GameService::class)->find($id);
        if (!$game->getCanChange()) {
            $this->addFlash('info', 'В игре нельзя менять проект');
            return $this->redirectToRoute('app_game_show', ['id' => $id]);
        }
        $player = $game->getPlayerForUser($this->getUser());
        $currentProjects = $this->container->get(ProjectService::class)->getCurrent($this->getUser(), true);
        if ($player->getChanges() > 0) {
            $changeDate = date_create_from_format('Y-m-d H:i:s', $player->getLastProjects()[count($player->getLastProjects()) - 1]['date']);
            if ($changeDate > $game->lastStepDate()) {
                $this->addFlash('info', 'Вы уже меняли проект на этой отчетной неделе.');
                return $this->redirectToRoute('app_game_show', ['id' => $id]);
            }
        }
        if (count($currentProjects) === 0) {
            $this->addFlash('info', 'Вы не можете сменить проект. У вас нет больше текущих проектов вне игр');
            return $this->redirectToRoute('app_game_show', ['id' => $id]);
        }
        $form = $this->createFormBuilder()
            ->add('project', EntityType::class, [
                'class' => Project::class,
                'choices' => $currentProjects
            ])->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $lastProjects = $player->getLastProjects();
            $fromProject = $player->getProject();
            array_push($lastProjects, [
                'id' => $fromProject->getId(),
                'date' => (new \DateTime())->format('Y-m-d H:i:s'),
                'done' => false
            ]);
            $player->setLastProjects($lastProjects);
            $player->setProject($form->get('project')->getData());
            $em = $this->getDoctrine()->getManager();
            $em->persist($player);
            $em->flush();
            $this->addFlash('info', 'Вы сменили проект в игре');
            $this->get(CommentService::class)->addComment('Сменил проект с ' . $fromProject->getName() . ' на ' . $player->getProject()->getName(), $this->getUser(), $game);
            return $this->redirectToRoute('app_game_show', ['id' => $id]);
        }
        return $this->render('/game/change.html.twig', [
            'game' => $game,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{player_id}/reward")
     * @IsGranted("ROLE_USER")
     */
    public function reward($player_id, Request $request)
    {
        /** @var Player $player */
        $player = $this->get(PlayerService::class)->find($player_id);
        /** @var Game $game */
        $game = $player->getGame();
        $this->checkAccess($game);
        if ($request->isMethod('POST')) {
            $reward = new Reward();
            $reward->setPlayer($player);
            $reward->setRewardType($this->get(RewardTypeService::class)->find($request->request->getInt('type')));
            $em = $this->getDoctrine()->getManager();
            $em->persist($reward);
            $em->flush();
            $this->addFlash('info', 'Награда присвоена');
            return $this->redirectToRoute('app_game_steps', ['id' => $game->getId()]);
        }
        return $this->render('game/reward.html.twig', [
            'player' => $player,
            'types' => $this->get(RewardTypeService::class)->getActive()
        ]);
    }

    /**
     * @Route("/{reward_id}/reward/cancel")
     * @IsGranted("ROLE_USER")
     */
    public function cancelReward($reward_id, Request $request)
    {
        $reward = $this->getDoctrine()->getRepository(Reward::class)->find($reward_id);
        /** @var Game $game */
        $game = $reward->getPlayer()->getGame();
        $this->checkAccess($game);
        $em = $this->getDoctrine()->getManager();
        $em->remove($reward);
        $em->flush();
        $this->addFlash('info', 'Награда отклонена');
        return $this->redirectToRoute('app_game_steps', ['id' => $game->getId()]);
    }

    /**
     * @Route("/{id}/gallery")
     */
    public function gallery($id)
    {
        /** @var Game $game */
        $game = $this->get(GameService::class)->find($id);
        return $this->render('/game/gallery.html.twig', [
            'game' => $game,
            'projects' => $this->get(GameService::class)->getDoneProjects($game),
        ]);
    }
}
