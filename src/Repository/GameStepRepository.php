<?php

namespace App\Repository;

use App\Entity\GameStep;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method GameStep|null find($id, $lockMode = null, $lockVersion = null)
 * @method GameStep|null findOneBy(array $criteria, array $orderBy = null)
 * @method GameStep[]    findAll()
 * @method GameStep[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameStepRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, GameStep::class);
    }

//    /**
//     * @return GameStep[] Returns an array of GameStep objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GameStep
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
