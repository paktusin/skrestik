<?php

namespace App\Repository;

use App\Entity\PlayerStep;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PlayerStep|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlayerStep|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlayerStep[]    findAll()
 * @method PlayerStep[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlayerStepRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PlayerStep::class);
    }

//    /**
//     * @return PlayerStep[] Returns an array of PlayerStep objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlayerStep
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
