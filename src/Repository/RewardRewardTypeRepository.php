<?php

namespace App\Repository;

use App\Entity\RewardType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method RewardType|null find($id, $lockMode = null, $lockVersion = null)
 * @method RewardType|null findOneBy(array $criteria, array $orderBy = null)
 * @method RewardType[]    findAll()
 * @method RewardType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RewardRewardTypeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RewardType::class);
    }

//    /**
//     * @return Reward[] Returns an array of Reward objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Reward
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
