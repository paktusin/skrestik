<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180929190855 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
$this->addSql("
update base set coef = 0.254000508 where id = 49;
update base set coef = 0.23090902 where id = 3;
update base set coef = 0.211666642 where id = 44;
update base set coef = 0.195384624 where id = 46;
update base set coef = 0.181428605 where id = 4;
update base set coef = 0.169333385 where id = 47;
update base set coef = 0.158750065 where id = 5;
update base set coef = 0.141111194 where id = 6;
update base set coef = 0.126999931 where id = 50;
update base set coef = 0.11545451 where id = 7;
update base set coef = 0.141111194 where id = 8;
update base set coef = 0.126999931 where id = 9;
update base set coef = 0.11545451 where id = 10;
update base set coef = 0.101599997 where id = 17;
");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
