<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180923175320 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project CHANGE start start DATETIME DEFAULT NULL, CHANGE cross_x cross_x INT DEFAULT NULL, CHANGE cross_y cross_y INT DEFAULT NULL, CHANGE cross_count cross_count INT DEFAULT NULL, CHANGE color_count color_count INT DEFAULT NULL, CHANGE base_color base_color VARCHAR(255) DEFAULT NULL, CHANGE start_done_cross start_done_cross INT DEFAULT NULL, CHANGE end_plan end_plan DATE DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project CHANGE start start DATETIME NOT NULL, CHANGE cross_x cross_x INT NOT NULL, CHANGE cross_y cross_y INT NOT NULL, CHANGE cross_count cross_count INT NOT NULL, CHANGE color_count color_count INT NOT NULL, CHANGE base_color base_color VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE start_done_cross start_done_cross INT NOT NULL, CHANGE end_plan end_plan DATE NOT NULL');
    }
}
