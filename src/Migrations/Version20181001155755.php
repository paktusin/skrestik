<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181001155755 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE game (id INT AUTO_INCREMENT NOT NULL, organizer_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, start DATETIME NOT NULL, cross_limit INT NOT NULL, penalty_limit INT NOT NULL, player_limit INT NOT NULL, cross_norm INT NOT NULL, type SMALLINT NOT NULL, step_day SMALLINT NOT NULL, end DATETIME DEFAULT NULL, can_change TINYINT(1) NOT NULL, can_join TINYINT(1) NOT NULL, need_photo TINYINT(1) NOT NULL, INDEX IDX_232B318C876C4DDA (organizer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reward (id INT AUTO_INCREMENT NOT NULL, player_id INT DEFAULT NULL, reward_type_id INT DEFAULT NULL, created_at DATETIME NOT NULL, INDEX IDX_4ED1725399E6F5DF (player_id), INDEX IDX_4ED172532FFB716B (reward_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reward_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, enabled TINYINT(1) NOT NULL, image VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE player (id INT AUTO_INCREMENT NOT NULL, game_id INT DEFAULT NULL, user_id INT DEFAULT NULL, game_over TINYINT(1) NOT NULL, INDEX IDX_98197A65E48FD905 (game_id), INDEX IDX_98197A65A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE game_step (id INT AUTO_INCREMENT NOT NULL, player_id INT DEFAULT NULL, game_id INT DEFAULT NULL, date DATETIME NOT NULL, cross_count INT NOT NULL, penalty INT NOT NULL, comment LONGTEXT NOT NULL, INDEX IDX_A8AC921099E6F5DF (player_id), INDEX IDX_A8AC9210E48FD905 (game_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318C876C4DDA FOREIGN KEY (organizer_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE reward ADD CONSTRAINT FK_4ED1725399E6F5DF FOREIGN KEY (player_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE reward ADD CONSTRAINT FK_4ED172532FFB716B FOREIGN KEY (reward_type_id) REFERENCES reward_type (id)');
        $this->addSql('ALTER TABLE player ADD CONSTRAINT FK_98197A65E48FD905 FOREIGN KEY (game_id) REFERENCES game (id)');
        $this->addSql('ALTER TABLE player ADD CONSTRAINT FK_98197A65A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE game_step ADD CONSTRAINT FK_A8AC921099E6F5DF FOREIGN KEY (player_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE game_step ADD CONSTRAINT FK_A8AC9210E48FD905 FOREIGN KEY (game_id) REFERENCES game (id)');
        $this->addSql('ALTER TABLE project ADD player_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE99E6F5DF FOREIGN KEY (player_id) REFERENCES player (id)');
        $this->addSql('CREATE INDEX IDX_2FB3D0EE99E6F5DF ON project (player_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE player DROP FOREIGN KEY FK_98197A65E48FD905');
        $this->addSql('ALTER TABLE game_step DROP FOREIGN KEY FK_A8AC9210E48FD905');
        $this->addSql('ALTER TABLE reward DROP FOREIGN KEY FK_4ED172532FFB716B');
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EE99E6F5DF');
        $this->addSql('ALTER TABLE reward DROP FOREIGN KEY FK_4ED1725399E6F5DF');
        $this->addSql('ALTER TABLE game_step DROP FOREIGN KEY FK_A8AC921099E6F5DF');
        $this->addSql('DROP TABLE game');
        $this->addSql('DROP TABLE reward');
        $this->addSql('DROP TABLE reward_type');
        $this->addSql('DROP TABLE player');
        $this->addSql('DROP TABLE game_step');
        $this->addSql('DROP INDEX IDX_2FB3D0EE99E6F5DF ON project');
        $this->addSql('ALTER TABLE project DROP player_id');
    }
}
