<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181011192024 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE player_step (id INT AUTO_INCREMENT NOT NULL, player_id INT DEFAULT NULL, project_id INT DEFAULT NULL, game_step_id INT DEFAULT NULL, count INT NOT NULL, penalty INT NOT NULL, comment LONGTEXT DEFAULT NULL, INDEX IDX_E1A3AE0F99E6F5DF (player_id), INDEX IDX_E1A3AE0F166D1F9C (project_id), INDEX IDX_E1A3AE0FE67061B3 (game_step_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE player_step ADD CONSTRAINT FK_E1A3AE0F99E6F5DF FOREIGN KEY (player_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE player_step ADD CONSTRAINT FK_E1A3AE0F166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE player_step ADD CONSTRAINT FK_E1A3AE0FE67061B3 FOREIGN KEY (game_step_id) REFERENCES game_step (id)');
        $this->addSql('ALTER TABLE game_step DROP FOREIGN KEY FK_A8AC921099E6F5DF');
        $this->addSql('DROP INDEX IDX_A8AC921099E6F5DF ON game_step');
        $this->addSql('ALTER TABLE game_step ADD steps_id INT DEFAULT NULL, DROP cross_count, DROP penalty, DROP comment, CHANGE player_id game_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE game_step ADD CONSTRAINT FK_A8AC9210E48FD905 FOREIGN KEY (game_id) REFERENCES game (id)');
        $this->addSql('ALTER TABLE game_step ADD CONSTRAINT FK_A8AC92101EBBD054 FOREIGN KEY (steps_id) REFERENCES player_step (id)');
        $this->addSql('CREATE INDEX IDX_A8AC9210E48FD905 ON game_step (game_id)');
        $this->addSql('CREATE INDEX IDX_A8AC92101EBBD054 ON game_step (steps_id)');
        $this->addSql('ALTER TABLE player DROP changes');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE game_step DROP FOREIGN KEY FK_A8AC92101EBBD054');
        $this->addSql('DROP TABLE player_step');
        $this->addSql('ALTER TABLE game_step DROP FOREIGN KEY FK_A8AC9210E48FD905');
        $this->addSql('DROP INDEX IDX_A8AC9210E48FD905 ON game_step');
        $this->addSql('DROP INDEX IDX_A8AC92101EBBD054 ON game_step');
        $this->addSql('ALTER TABLE game_step ADD player_id INT DEFAULT NULL, ADD cross_count INT NOT NULL, ADD penalty INT NOT NULL, ADD comment LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, DROP game_id, DROP steps_id');
        $this->addSql('ALTER TABLE game_step ADD CONSTRAINT FK_A8AC921099E6F5DF FOREIGN KEY (player_id) REFERENCES player (id)');
        $this->addSql('CREATE INDEX IDX_A8AC921099E6F5DF ON game_step (player_id)');
        $this->addSql('ALTER TABLE player ADD changes INT NOT NULL');
    }
}
