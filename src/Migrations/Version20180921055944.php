<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180921055944 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("
        INSERT INTO base(id,name,type) VALUES (1,'не знаю',NULL);
INSERT INTO base(id,name,type) VALUES (2,'нет в списке',NULL);
INSERT INTO base(id,name,type) VALUES (49,'канва Аида 10','канва');
INSERT INTO base(id,name,type) VALUES (3,'канва Аида 11','канва');
INSERT INTO base(id,name,type) VALUES (44,'канва Аида 12','канва');
INSERT INTO base(id,name,type) VALUES (46,'канва Аида 13','канва');
INSERT INTO base(id,name,type) VALUES (4,'канва Аида 14','канва');
INSERT INTO base(id,name,type) VALUES (47,'канва Аида 15','канва');
INSERT INTO base(id,name,type) VALUES (5,'канва Аида 16','канва');
INSERT INTO base(id,name,type) VALUES (6,'канва Аида 18','канва');
INSERT INTO base(id,name,type) VALUES (50,'канва Аида 20','канва');
INSERT INTO base(id,name,type) VALUES (7,'канва Хардангер 22','канва');
INSERT INTO base(id,name,type) VALUES (8,'ткань Davosa 18','ткани');
INSERT INTO base(id,name,type) VALUES (9,'ткань Bellana 20','ткани');
INSERT INTO base(id,name,type) VALUES (10,'ткань Janina 22','ткани');
INSERT INTO base(id,name,type) VALUES (17,'лен Dublin 25','ткани');
INSERT INTO base(id,name,type) VALUES (51,'ткань Evenweave 25','ткани');
INSERT INTO base(id,name,type) VALUES (11,'ткань Lugana 25','ткани');
INSERT INTO base(id,name,type) VALUES (12,'ткань Linda 27','ткани');
INSERT INTO base(id,name,type) VALUES (52,'ткань Evenweave 28','ткани');
INSERT INTO base(id,name,type) VALUES (53,'ткань Annabelle 28','ткани');
INSERT INTO base(id,name,type) VALUES (42,'двунитка 28','ткани');
INSERT INTO base(id,name,type) VALUES (13,'ткань Jobelan 28','ткани');
INSERT INTO base(id,name,type) VALUES (18,'лен Cashel 28','ткани');
INSERT INTO base(id,name,type) VALUES (54,'ткань Brittney Lugana 28','ткани');
INSERT INTO base(id,name,type) VALUES (41,'лен Permin 28','ткани');
INSERT INTO base(id,name,type) VALUES (45,'лен 30','ткани');
INSERT INTO base(id,name,type) VALUES (40,'лен Permin 32','ткани');
INSERT INTO base(id,name,type) VALUES (15,'ткань Belfast 32','ткани');
INSERT INTO base(id,name,type) VALUES (14,'ткань Murano 32','ткани');
INSERT INTO base(id,name,type) VALUES (16,'ткань Edinburgh 36','ткани');
INSERT INTO base(id,name,type) VALUES (19,'лен Newcastle 40','ткани');
INSERT INTO base(id,name,type) VALUES (21,'страмин','прочее');
INSERT INTO base(id,name,type) VALUES (20,'пластиковая канва','прочее');
INSERT INTO base(id,name,type) VALUES (48,'перфорированная бумага','прочее');
INSERT INTO base(id,name,type) VALUES (43,'неопознанная ткань','прочее');
        ");

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
