<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180921060013 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("
INSERT INTO thread(id,name,type) VALUES (1,'не знаю',NULL);
INSERT INTO thread(id,name,type) VALUES (31,'бисер','разное');
INSERT INTO thread(id,name,type) VALUES (3,'из набора','разное');
INSERT INTO thread(id,name,type) VALUES (4,'остатки','разное');
INSERT INTO thread(id,name,type) VALUES (5,'разных палитр','разное');
INSERT INTO thread(id,name,type) VALUES (30,'швейные','разное');
INSERT INTO thread(id,name,type) VALUES (6,'шерсть','разное');
INSERT INTO thread(id,name,type) VALUES (7,'Anchor','мулине');
INSERT INTO thread(id,name,type) VALUES (8,'Ariadna','мулине');
INSERT INTO thread(id,name,type) VALUES (9,'Belka','мулине');
INSERT INTO thread(id,name,type) VALUES (10,'Bestex','мулине');
INSERT INTO thread(id,name,type) VALUES (11,'Bucilla','мулине');
INSERT INTO thread(id,name,type) VALUES (12,'Candamar','мулине');
INSERT INTO thread(id,name,type) VALUES (13,'Cosmo','мулине');
INSERT INTO thread(id,name,type) VALUES (14,'Dimensions','мулине');
INSERT INTO thread(id,name,type) VALUES (15,'DMC','мулине');
INSERT INTO thread(id,name,type) VALUES (16,'DOME','мулине');
INSERT INTO thread(id,name,type) VALUES (17,'Finca','мулине');
INSERT INTO thread(id,name,type) VALUES (28,'Ideal','мулине');
INSERT INTO thread(id,name,type) VALUES (18,'J&P Coats','мулине');
INSERT INTO thread(id,name,type) VALUES (19,'Kreinik','мулине');
INSERT INTO thread(id,name,type) VALUES (20,'Madeira','мулине');
INSERT INTO thread(id,name,type) VALUES (21,'Mayflower','мулине');
INSERT INTO thread(id,name,type) VALUES (22,'Olympus','мулине');
INSERT INTO thread(id,name,type) VALUES (23,'Venus','мулине');
INSERT INTO thread(id,name,type) VALUES (24,'Гамма','мулине');
INSERT INTO thread(id,name,type) VALUES (25,'китайские','мулине');
INSERT INTO thread(id,name,type) VALUES (26,'корейские','мулине');
INSERT INTO thread(id,name,type) VALUES (29,'Орлис','мулине');
INSERT INTO thread(id,name,type) VALUES (27,'ПНК им. Кирова','мулине');
");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
