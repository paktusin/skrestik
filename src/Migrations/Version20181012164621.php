<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181012164621 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE game_step DROP FOREIGN KEY FK_A8AC92101EBBD054');
        $this->addSql('DROP INDEX IDX_A8AC92101EBBD054 ON game_step');
        $this->addSql('ALTER TABLE game_step DROP steps_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE game_step ADD steps_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE game_step ADD CONSTRAINT FK_A8AC92101EBBD054 FOREIGN KEY (steps_id) REFERENCES player_step (id)');
        $this->addSql('CREATE INDEX IDX_A8AC92101EBBD054 ON game_step (steps_id)');
    }
}
