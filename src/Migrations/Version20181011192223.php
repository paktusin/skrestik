<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181011192223 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE game CHANGE cross_limit cross_limit INT DEFAULT NULL, CHANGE penalty_limit penalty_limit INT DEFAULT NULL, CHANGE player_limit player_limit INT DEFAULT NULL, CHANGE cross_norm cross_norm INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE game CHANGE cross_limit cross_limit INT NOT NULL, CHANGE penalty_limit penalty_limit INT NOT NULL, CHANGE player_limit player_limit INT NOT NULL, CHANGE cross_norm cross_norm INT NOT NULL');
    }
}
