<?php
/**
 * Created by PhpStorm.
 * User: davlichin.ma
 * Date: 11.08.2017
 * Time: 14:07
 */

namespace App\EventListener;

use App\Entity\File;
use App\Entity\Image;
use App\Entity\Progress;
use App\Entity\Project;
use App\Service\ImageService;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProgressListener
{

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $progress = $args->getEntity();
        if ($progress instanceof Progress) {
            $progress->setUpdatedAt(new \DateTime());
        };
    }

}