<?php
/**
 * Created by PhpStorm.
 * User: davlichin.ma
 * Date: 11.08.2017
 * Time: 14:07
 */

namespace App\EventListener;

use App\Entity\File;
use App\Entity\Image;
use App\Entity\Project;
use App\Service\ImageService;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImageListener
{

    private $cont;
    private $delImage;

    public function __construct(ContainerInterface $container)
    {
        $this->cont = $container;
        $this->em = $container;
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        if ($args->hasChangedField('image')) {
            $this->delImage = $args->getOldValue('image');
        };
    }

    public function postFlush(PostFlushEventArgs $args)
    {
        if ($this->delImage) {
            $em = $args->getEntityManager();
            $em->remove($this->delImage);
            $this->delImage = null;
            $em->flush();
        }
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof Image) {
            $filename = $this->cont->get('kernel')->getRootDir() . '/../public/upload/' . $entity->getName();
            if (file_exists($filename)) {
                unlink($filename);
            }
        };
    }

}