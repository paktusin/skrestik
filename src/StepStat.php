<?php
/**
 * Created by PhpStorm.
 * User: paktus
 * Date: 10.10.18
 * Time: 21:09
 */

namespace App;


use App\Entity\Player;
use App\Entity\Project;
use App\Entity\User;

class StepStat
{
    /** @var User $player */
    public $user;
    /** @var Player $player */
    public $player;
    /** @var Project $player */
    public $project;
    public $prevCross = 0;
    public $Cross = 0;
    public $photo = null;
    public $penalty = 0;
    public $comment = '';
}