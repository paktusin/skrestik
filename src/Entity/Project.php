<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }


    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $parse_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Author")
     */
    private $author;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Thread")
     */
    private $thread;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Base")
     */
    private $base;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="projects")
     */
    private $user;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    private $start;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $crossX;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $crossY;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $crossCount;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $colorCount;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $baseColor;

    /**
     * @ORM\Column(type="boolean")
     */
    private $hidden;

    /**
     * @ORM\Column(type="boolean")
     */
    private $disableComments;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $startDoneCross;

    /**
     * @ORM\Column(type="date",nullable=true)
     */
    private $endPlan;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $end;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Image", cascade={"all"})
     */
    private $image;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Image", cascade={"all"})
     * @ORM\OrderBy({"created_at" = "DESC"})
     */
    private $images;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="project",cascade={"all"})
     * @ORM\OrderBy({"created_at"="DESC"})
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Progress",  mappedBy="project",cascade={"all"})
     * @ORM\OrderBy({"date" = "ASC"})
     */
    private $progress;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="watch_projects")
     */
    private $watchers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Player", mappedBy="project")
     */
    private $players;

    public function __construct()
    {
        $this->hidden = false;
        $this->disableComments = false;
        $this->images = new ArrayCollection();
        $this->players = new ArrayCollection();
        $this->progress = new ArrayCollection();
        $this->watchers = new ArrayCollection();
        $this->crossCount = 0;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return Author
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param Author $author
     */
    public function setAuthor($author): void
    {
        $this->author = $author;
    }

    /**
     * @return Thread
     */
    public function getThread()
    {
        return $this->thread;
    }

    /**
     * @param Thread $thread
     */
    public function setThread($thread): void
    {
        $this->thread = $thread;
    }

    /**
     * @return Base
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * @param Base $base
     */
    public function setBase($base): void
    {
        $this->base = $base;
    }

    /**
     * @return mixed
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param mixed $start
     */
    public function setStart($start): void
    {
        $this->start = $start;
    }

    /**
     *
     */
    public function getSize()
    {
        return ($this->crossX && $this->crossX > 0 && $this->crossY && $this->crossY > 0) ? "{$this->crossX}x{$this->crossY}" : 'неизвестно';
    }

    /**
     * @return mixed
     */
    public function getCrossX()
    {
        return $this->crossX;
    }

    /**
     * @param mixed $crossX
     */
    public function setCrossX($crossX): void
    {
        $this->crossX = $crossX;
    }

    /**
     * @return mixed
     */
    public function getCrossY()
    {
        return $this->crossY;
    }

    /**
     * @param mixed $crossY
     */
    public function setCrossY($crossY): void
    {
        $this->crossY = $crossY;
    }

    /**
     * @return mixed
     */
    public function getCrossCount()
    {
        return intval($this->crossCount);
    }

    /**
     * @param mixed $crossCount
     */
    public function setCrossCount($crossCount): void
    {
        $this->crossCount = $crossCount;
    }

    /**
     * @return mixed
     */
    public function getColorCount()
    {
        return $this->colorCount;
    }

    /**
     * @param mixed $colorCount
     */
    public function setColorCount($colorCount): void
    {
        $this->colorCount = $colorCount;
    }

    /**
     * @return mixed
     */
    public function getBaseColor()
    {
        return $this->baseColor;
    }

    /**
     * @param mixed $baseColor
     */
    public function setBaseColor($baseColor): void
    {
        $this->baseColor = $baseColor;
    }

    /**
     * @return mixed
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * @param mixed $hidden
     */
    public function setHidden($hidden): void
    {
        $this->hidden = $hidden;
    }

    /**
     * @return mixed
     */
    public function getDisableComments()
    {
        return $this->disableComments;
    }

    /**
     * @param mixed $disableComments
     */
    public function setDisableComments($disableComments): void
    {
        $this->disableComments = $disableComments;
    }

    /**
     * @return mixed
     */
    public function getStartDoneCross()
    {
        return $this->startDoneCross ? $this->startDoneCross : 0;
    }

    /**
     * @param mixed $startDoneCross
     */
    public function setStartDoneCross($startDoneCross): void
    {
        $this->startDoneCross = $startDoneCross;
    }

    /**
     * @return \DateTime | null
     */
    public function getEndPlan()
    {
        return $this->endPlan;
    }

    /**
     * @param mixed $endPlan
     */
    public function setEndPlan($endPlan): void
    {
        $this->endPlan = $endPlan;
    }

    /**
     * @return mixed
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param mixed $end
     */
    public function setEnd($end): void
    {
        $this->end = $end;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getImages()
    {
        return $this->images;
    }

    public function addImage(?Image $image): void
    {
        if (!$image) return;
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
        }
    }

    public function removeImage(?Image $image): void
    {
        if (!$image) return;
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
        }
    }

    /**
     * @return mixed
     */
    public function getParseId()
    {
        return $this->parse_id;
    }

    /**
     * @param mixed $parse_id
     */
    public function setParseId($parse_id): void
    {
        $this->parse_id = $parse_id;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getComments()
    {
        return $this->comments;
    }


    /**
     * @return ArrayCollection
     */
    public function getProgress()
    {
        return $this->progress;
    }

    public function progressValue()
    {
        if ($this->getCrossCount() === 0) return 100;
        $res = round(100 * $this->progressCount() / $this->getCrossCount());
        if ($res > 100) return 100;
        return $res;
    }

    public function progressCount()
    {
        return array_reduce($this->getProgress()->toArray(), function ($sum, Progress $progress) {
                return $progress->getCount() + $sum;
            }, 0) + $this->getStartDoneCross();
    }

    public function progressCountLeft()
    {
        $res = $this->getCrossCount() - $this->progressCount();
        return $res > 0 ? $res : 0;
    }

    public function progressDaysLeft()
    {
        if ($this->progressSpeed() == 0) return 0;
        return round($this->progressCountLeft() / $this->progressSpeed());
    }

    public function mustSpeed()
    {
        $now = (new \DateTime())->setTime(0, 0, 0);
        if (is_null($this->endPlan) || $this->endPlan < $now) return 0;
        $days = $this->getEndPlan()->diff($now)->days;
        return round($this->progressCountLeft() / ($days <= 0 ? 1 : $days));
    }

    public function progressSpeed()
    {
        if ($this->progressDaysCount() == 0) return 0;
        return round($this->progressCount() / $this->progressDaysCount());
    }

    public function progressPlanEnd()
    {
        return (new \DateTime())->add(\DateInterval::createFromDateString("{$this->progressDaysLeft()}days"));
    }

    public function progressDaysCount()
    {
        if (!$this->start) return 0;
        return (new \DateTime())->diff($this->start)->days;
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    public function isCurrent()
    {
        return $this->start !== null and $this->end === null;
    }

    /**
     * @return ArrayCollection
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * @return ArrayCollection
     */
    public function getActivePlayers()
    {
        return $this->players->filter(function (Player $player) {
            return !$player->getGameOver();
        });
    }

    /**
     * @return Image | null
     */
    public function lastPhoto()
    {
        if ($this->images->count() > 0) {
            return $this->images->first();
        } else {
            return null;
        }
    }

    /**
     * @param GameStep | null $gameStep
     * @param bool $after
     * @return Image | null
     * @throws \Exception
     */
    public function photo($gameStep, $after = false)
    {
        if (is_null($gameStep)) return null;
        $start = null;
        if ($after) {
            $start = $gameStep->getDate();
            $end = new \DateTime();
        } else {
            $before = $gameStep->getGame()->beforeStep($gameStep);
            if ($before) $start = $before->getDate();
            $end = $gameStep->getDate();
        }
        return $this->periodPhoto($start, $end);
    }

    /**
     * @param \DateTime|null $start
     * @param \DateTime|null $end
     * @return Image | null
     */
    public function periodPhoto($start = null, $end = null)
    {
        if (!$end) return $this->lastPhoto();
        $img = null;
        $filtered = $this->images->filter(function (Image $image) use ($end, $start) {
            return (is_null($start) || $start < $image->getCreatedAt()) && $image->getCreatedAt() <= $end;
        });
        return $filtered->count() > 0 ? $filtered->first() : null;
    }

    /**
     * @return mixed
     */
    public function getWatchers()
    {
        return $this->watchers;
    }

    public function isDone()
    {
        return !is_null($this->getEnd()) && $this->getEnd() < (new \DateTime());
    }

}
