<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlayerStepRepository")
 */
class PlayerStep
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $count = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $penalty = 0;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Player", inversedBy="steps")
     */
    private $player;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Project")
     */
    private $project;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\GameStep", inversedBy="steps")
     */
    private $gameStep;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    /**
     * @return int|null
     */
    public function previousCount(): ?int
    {
        /** @var PlayerStep[] $prev */
        $prevGameStep = $this->getGameStep()->getGame()->beforeStep($this->getGameStep());
        if (!$prevGameStep) return 0;
        $prev = $prevGameStep->getSteps()->filter(function (PlayerStep $step) {
            return $step->getPlayer() === $this->getPlayer();
        })->getValues();
        return count($prev) > 0 ? $prev[0]->getCount() : 0;
    }

    public function setCount(int $count): self
    {
        $this->count = $count;

        return $this;
    }

    public function getPenalty(): ?int
    {
        return $this->penalty;
    }

    public function setPenalty(int $penalty): self
    {
        $this->penalty = $penalty;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return Player
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * @param Player $player
     */
    public function setPlayer($player): void
    {
        $this->player = $player;
    }

    /**
     * @return GameStep
     */
    public function getGameStep()
    {
        return $this->gameStep;
    }

    /**
     * @param GameStep $gameStep
     */
    public function setGameStep($gameStep): void
    {
        $this->gameStep = $gameStep;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject($project): void
    {
        $this->project = $project;
    }
}
