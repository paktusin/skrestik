<?php
/**
 * Created by PhpStorm.
 * User: paktus
 * Date: 20.09.18
 * Time: 13:39
 */

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_group")
 */
class Group extends \FOS\UserBundle\Model\Group
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
}