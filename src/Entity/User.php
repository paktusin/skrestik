<?php
/**
 * Created by PhpStorm.
 * User: paktus
 * Date: 20.09.18
 * Time: 13:39
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends \FOS\UserBundle\Model\User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $parse_id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Player",mappedBy="user", cascade={"remove"})
     */
    private $players;

    public function __construct()
    {
        parent::__construct();
        $this->created_at = new \DateTime();
        $this->friends = new ArrayCollection();
        $this->projects = new ArrayCollection();
        $this->outRequests = new ArrayCollection();
        $this->inRequests = new ArrayCollection();
        $this->watch_projects = new ArrayCollection();
        $this->inMessages = new ArrayCollection();
        $this->outMessages = new ArrayCollection();
        $this->myGames = new ArrayCollection();
        $this->players = new ArrayCollection();
        $this->notify = true;
    }

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Group")
     * @ORM\JoinTable(name="fos_user_group",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     */
    protected $groups;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User")
     */
    private $friends;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Message",mappedBy="to",cascade={"remove"})
     */
    private $inMessages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment",mappedBy="user",cascade={"remove"})
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Message",mappedBy="from",cascade={"remove"})
     */
    private $outMessages;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $birthdate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Image",cascade={"all"})
     */
    private $image;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="boolean")
     */
    private $notify;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $social;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Project", inversedBy="watchers")
     */
    private $watch_projects;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Project",mappedBy="user",cascade={"remove"})
     */
    private $projects;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FriendRequest",mappedBy="from",cascade={"remove"})
     */
    private $outRequests;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Game",mappedBy="organizer")
     */
    private $myGames;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FriendRequest",mappedBy="to",cascade={"remove"})
     */
    private $inRequests;

    /**
     * @return \DateTime|null
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    public function getYears()
    {
        return $this->birthdate ? (new \DateTime())->diff($this->birthdate)->y : null;
    }


    /**
     * @param \DateTime $birthdate
     */
    public function setBirthdate($birthdate): void
    {
        $this->birthdate = $birthdate;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city): void
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name ? $this->name : $this->username;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSocial()
    {
        return $this->social;
    }

    /**
     * @param mixed $social
     */
    public function setSocial($social): void
    {
        $this->social = $social;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt(\DateTime $created_at): void
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param Image|null $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getParseId()
    {
        return $this->parse_id;
    }

    /**
     * @param mixed $parse_id
     */
    public function setParseId($parse_id): void
    {
        $this->parse_id = $parse_id;
    }

    /**
     * @return mixed
     */
    public function getFriends()
    {
        return $this->friends;
    }

    public function addFriend(User $user): void
    {
        if (!$this->friends->contains($user)) {
            $this->friends[] = $user;
        }
    }

    public function removeFriend(User $user): void
    {
        if ($this->friends->contains($user)) {
            $this->friends->removeElement($user);
        }
    }

    /**
     * @return ArrayCollection
     */
    public function getWatchProjects()
    {
        return $this->watch_projects;
    }

    public function addWatchProject(Project $project)
    {
        if (!$this->watch_projects->contains($project)) {
            $this->watch_projects[] = $project;
        }
    }

    public function removeWatchProject(Project $project): void
    {
        if ($this->watch_projects->contains($project)) {
            $this->watch_projects->removeElement($project);
        }
    }


//    public function speed()
//    {
//        $speeds = $this->getProjects()->map(function (Project $project) {
//            return $project->progressSpeed();
//        })->toArray();
//        if (count($speeds) === 0) return 0;
//        return round(array_reduce($speeds, function ($prev, $speed) {
//                return $prev + $speed;
//            }, 0) / count($speeds));
//    }

    /**
     * @return ArrayCollection
     */
    public function getProjects()
    {
        return $this->projects;
    }

    public function hasFriend(User $user)
    {
        return $this->friends->contains($user);
    }

    public function hasRequestTo(User $user)
    {
        return $this->getOutRequests()->filter(function (FriendRequest $friendRequest) use ($user) {
                return $friendRequest->getTo() === $user;
            })->count() > 0;
    }


    /**
     * @return ArrayCollection
     */
    public function getOutRequests()
    {
        return $this->outRequests;
    }

    /**
     * @return mixed
     */
    public function getInRequests()
    {
        return $this->inRequests;
    }

    /**
     * @return mixed
     */
    public function getInMessages()
    {
        return $this->inMessages;
    }

    /**
     * @return mixed
     */
    public function getOutMessages()
    {
        return $this->outMessages;
    }

    /**
     * @return mixed
     */
    public function getNotify()
    {
        return $this->notify;
    }

    /**
     * @param mixed $notify
     */
    public function setNotify($notify): void
    {
        $this->notify = $notify;
    }

    /**
     * @return ArrayCollection
     */
    public function getGames()
    {
        return $this->players->map(function (Player $player) {
            return $player->getGame();
        });
    }

    /**
     * @return ArrayCollection
     */
    public function getActiveGames()
    {
        return $this->getPlayers()->filter(function (Player $player) {
            return !$player->getGameOver();
        })->map(function (Player $player) {
            return $player->getGame();
        });
    }

    /**
     * @param bool $actual
     * @return ArrayCollection
     */
    public function getMyGames($actual = false)
    {
        if ($actual) {
            return $this->myGames->filter(function (Game $game) {
                return $game->isActual();
            });
        }
        return $this->myGames;
    }

    /**
     * @return ArrayCollection
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * @return mixed
     */
    public function getComments()
    {
        return $this->comments;
    }

    public function reward()
    {
        return array_reduce($this->getPlayers()->toArray(), function ($reward, Player $player) {
            if ($player->getRewards()->count() > 0) {
                /** @var Reward $newReward */
                $newReward = $player->getRewards()->first();
                if ($reward == null || $newReward->getCreatedAt() > $reward->getCreatedAt()) {
                    $reward = $newReward;
                }
            }
            return $reward;
        }, null);
    }

    /**
     * @param Game | null $game
     * @return mixed
     */
    public function rewards($game = null)
    {
        $players = $this->getPlayers();
        if ($game) {
            $players = $players->filter(function (Player $player) use ($game) {
                return $player->getGame() === $game;
            });
        }
        return array_reduce($players->toArray(), function ($rewards, Player $player) {
            return array_merge($player->getRewards()->toArray(), $rewards);
        }, []);
    }

    public function __toString()
    {
        return $this->getEmail() . ' ( ' . $this->getUsername() . ' )';
    }


}