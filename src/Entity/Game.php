<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Date;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GameRepository")
 */
class Game
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $short;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $start;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $crossLimit;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $penaltyLimit;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $playerLimit;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $crossNorm;

    /**
     * @ORM\Column(type="smallint")
     */
    private $type;

    /**
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $moderated;

    /**
     * @ORM\Column(type="smallint")
     */
    private $stepDay;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    private $end;

    /**
     * @ORM\Column(type="boolean")
     */
    private $canChange;

    /**
     * @ORM\Column(type="boolean")
     */
    private $canJoin;

    /**
     * @ORM\Column(type="boolean")
     */
    private $needPhoto;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="myGames")
     */
    private $organizer;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Player", mappedBy="game", cascade={"remove"})
     */
    private $players;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\GameStep", mappedBy="game", cascade={"remove"})
     * @ORM\OrderBy({"date" = "ASC"})
     */
    private $steps;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="game",cascade={"all"})
     * @ORM\OrderBy({"created_at"="DESC"})
     */
    private $comments;

    public function __construct()
    {
        $this->setStart((new \DateTime())->add(\DateInterval::createFromDateString('2 days')));
        $this->setType(1);
        $this->setStepDay(7);
        $this->setCrossLimit(100000);
        $this->setCrossNorm(200);
        $this->setPenaltyLimit(600);
        $this->setPlayerLimit(20);
        $this->setNeedPhoto(true);
        $this->setCanChange(true);
        $this->setCanJoin(true);
        $this->players = new ArrayCollection();
        $this->steps = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    public function active()
    {
        return $this->getModerated() && !$this->getEnd() || new \DateTime() < $this->getEnd();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStart(): ?\DateTime
    {
        return $this->start;
    }

    /**
     * @return \DateTime|null
     */
    public function lastStepDate()
    {
        if ($this->steps->count() > 0) {
            return $this->steps->last()->getDate();
        }
        return $this->getStart();
    }

    /**
     * @param GameStep $gameStep
     * @return GameStep | null
     */
    public function beforeStep(GameStep $gameStep)
    {
        $index = $this->steps->indexOf($gameStep);
        if ($index && $index > 0) {
            return $this->steps->get($index - 1);
        }
        return null;
    }

    /**
     * @return GameStep|null
     */
    public function lastStep()
    {
        if ($this->steps->count() > 0) {
            return $this->steps->last();
        }
        return null;
    }

    public function setStart(\DateTime $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): ?\DateTime
    {
        return $this->end;
    }

    /**
     * @param \DateTime|null $end
     * @return Game
     */
    public function setEnd($end): self
    {
        $this->end = $end;

        return $this;
    }

    public function getCanChange(): ?bool
    {
        return $this->canChange;
    }

    public function setCanChange(bool $canChange): self
    {
        $this->canChange = $canChange;

        return $this;
    }

    public function getCanJoin(): ?bool
    {
        return $this->canJoin;
    }

    public function setCanJoin(bool $canJoin): self
    {
        $this->canJoin = $canJoin;

        return $this;
    }

    public function getNeedPhoto(): ?bool
    {
        return $this->needPhoto;
    }

    public function setNeedPhoto(bool $needPhoto): self
    {
        $this->needPhoto = $needPhoto;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCrossLimit()
    {
        return $this->crossLimit;
    }

    /**
     * @param mixed $crossLimit
     */
    public function setCrossLimit($crossLimit): void
    {
        $this->crossLimit = $crossLimit;
    }

    /**
     * @return integer
     */
    public function getStepDay()
    {
        return $this->stepDay;
    }

    /**
     * @param mixed $stepDay
     */
    public function setStepDay($stepDay): void
    {
        $this->stepDay = $stepDay;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getCrossNorm()
    {
        return $this->crossNorm;
    }

    /**
     * @param mixed $crossNorm
     */
    public function setCrossNorm($crossNorm): void
    {
        $this->crossNorm = $crossNorm;
    }


    /**
     * @return mixed
     */
    public function getPenaltyLimit()
    {
        return $this->penaltyLimit;
    }

    /**
     * @param mixed $penaltyLimit
     */
    public function setPenaltyLimit($penaltyLimit): void
    {
        $this->penaltyLimit = $penaltyLimit;
    }

    /**
     * @return mixed
     */
    public function getPlayerLimit()
    {
        return $this->playerLimit;
    }

    /**
     * @param mixed $playerLimit
     */
    public function setPlayerLimit($playerLimit): void
    {
        $this->playerLimit = $playerLimit;
    }

    /**
     * @return mixed
     */
    public function getOrganizer()
    {
        return $this->organizer;
    }

    /**
     * @param mixed $organizer
     */
    public function setOrganizer($organizer): void
    {
        $this->organizer = $organizer;
    }

    /**
     * @return ArrayCollection
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * @return ArrayCollection
     */
    public function getActivePlayers()
    {
        return $this->getPlayers()->filter(function (Player $player) {
            return !$player->getGameOver();
        });
    }

    /**
     * @param GameStep $player
     * @return mixed
     */
    public function addPlayer($player)
    {
        if (!$this->players->contains($player)) {
            $this->players[] = $player;
        }
        return $this;
    }

    /**
     * @param GameStep $player
     * @return mixed
     */
    public function removePlayer($player)
    {
        if ($this->players->contains($player)) {
            return $this->players->removeElement($player);
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModerated()
    {
        return $this->moderated;
    }

    /**
     * @param mixed $moderated
     */
    public function setModerated($moderated): void
    {
        $this->moderated = $moderated;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function inGame(User $user)
    {
        return !is_null($this->getPlayerForUser($user));
    }

    /**
     * @param User $user
     * @return Player|null
     */
    public function getPlayerForUser(User $user)
    {
        $res = $this->players->filter(function (Player $player) use ($user) {
            return $player->getUser() === $user && !$player->getGameOver();
        });
        if ($res->count() > 0) {
            return $res->first();
        }
        return null;
    }

    /**
     * @return ArrayCollection
     */
    public function getSteps()
    {
        return $this->steps;
    }

    /**
     * @return mixed
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @return mixed
     */
    public function getShort()
    {
        return $this->short;
    }

    /**
     * @param mixed $short
     */
    public function setShort($short): void
    {
        $this->short = $short;
    }

    public function isActual()
    {
        return !$this->getEnd() || $this->getEnd() >= (new \DateTime());
    }
}
