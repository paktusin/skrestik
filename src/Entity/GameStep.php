<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GameStepRepository")
 */
class GameStep
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Game", inversedBy="steps")
     */
    private $game;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlayerStep", mappedBy="gameStep", cascade={"all"})
     */
    private $steps;

    /**
     * GameStep constructor.
     */
    public function __construct()
    {
        $this->steps = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    public function setDate(\DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Game
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * @param Game $game
     */
    public function setGame($game): void
    {
        $this->game = $game;
    }

    /**
     * @return ArrayCollection
     */
    public function getSteps()
    {
        return $this->steps;
    }

    /**
     * @param PlayerStep $playerStep
     * @return void
     */
    public function addStep(PlayerStep $playerStep)
    {
        if (!$this->steps->contains($playerStep)) {
            $playerStep->setGameStep($this);
            $this->steps[] = $playerStep;
        }
    }

    /**
     * @param PlayerStep $playerStep
     * @return void
     */
    public function returnStep(PlayerStep $playerStep)
    {
        if ($this->steps->contains($playerStep)) {
            $this->steps->removeElement($playerStep);
        }
    }
}
