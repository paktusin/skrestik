<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OrderBy;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlayerRepository")
 */
class Player
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $gameOver;

    /**
     * @ORM\Column(type="array")
     */
    private $lastProjects;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Game", inversedBy="players")
     */
    private $game;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="players")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="players")
     */
    private $project;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Reward", mappedBy="player", cascade={"remove"})
     * @ORM\OrderBy({"created_at" = "DESC"})
     */
    private $rewards;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlayerStep", mappedBy="player", cascade={"remove"})
     */
    private $steps;

    public function __construct()
    {
        $this->rewards = new ArrayCollection();
        $this->steps = new ArrayCollection();
        $this->gameOver = false;
        $this->setLastProjects([]);
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGameOver(): ?bool
    {
        return $this->gameOver;
    }

    public function setGameOver(bool $gameOver): self
    {
        $this->gameOver = $gameOver;

        return $this;
    }

    /**
     * @return Game
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * @param mixed $game
     */
    public function setGame($game): void
    {
        $this->game = $game;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }


    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @return ArrayCollection
     */
    public function getSteps()
    {
        return $this->steps;
    }

    /**
     * @param GameStep $gameStep
     * @return PlayerStep
     */
    public function getStepByStep(GameStep $gameStep)
    {
        $res = $this->steps->filter(function (PlayerStep $playerStep) use ($gameStep) {
            return $playerStep->getGameStep()->getId() === $gameStep->getId();
        });
        return ($res->count() > 0) ? $res->first() : null;
    }

    /**
     * @return PlayerStep | null
     */
    public function lastStep()
    {
        return $this->steps->count() > 0 ? $this->steps->last() : null;
    }

    /**
     * @return PlayerStep | null
     */
    public function prevStep()
    {
        return $this->steps->count() > 1 ? $this->steps[1] : null;
    }

    /**
     * @param PlayerStep $step
     * @return mixed
     */
    public function addStep($step)
    {
        if (!$this->steps->contains($step)) {
            $this->steps[] = $step;
        }
        return $this;
    }

    /**
     * @param PlayerStep $step
     * @return mixed
     */
    public function removeStep($step)
    {
        if ($this->steps->contains($step)) {
            $this->steps->removeElement($step);
        }
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getRewards()
    {
        return $this->rewards;
    }

    /**
     * @param mixed $project
     */
    public function setProject($project): void
    {
        $this->project = $project;
    }

    /**
     * @return array
     */
    public function getLastProjects()
    {
        return $this->lastProjects;
    }

    /**
     * @param array $lastProjects
     */
    public function setLastProjects($lastProjects): void
    {
        $this->lastProjects = $lastProjects;
    }

    /**
     * @return int
     */
    public function getChanges()
    {
        return count($this->getLastProjects());
    }

    public function __toString()
    {
        $game = $this->getGame() ? "Игра №{$this->getGame()->getId()}" : '';
        return (string)($this->getUser() ? $this->getUser()->getUserName() . ' ' . $game : '');
    }

    public function getLastProjectsInPeriod(\DateTime $startDate, \DateTime $stepDate)
    {
        return array_values(array_filter($this->getLastProjects(), function ($change) use ($stepDate, $startDate) {
            $changeDate = date_create_from_format('Y-m-d H:i:s', $change['date']);
            return $changeDate && $startDate < $changeDate && $changeDate < $stepDate;
        }));
    }

}
