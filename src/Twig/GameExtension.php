<?php

namespace App\Twig;

use App\Entity\Game;
use App\Entity\GameStep;
use App\Entity\Player;
use App\Entity\PlayerStep;
use App\Entity\Progress;
use App\Entity\Project;
use App\Entity\User;
use App\Service\GameService;
use App\Service\ProgressService;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraints\Date;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class GameExtension extends AbstractExtension
{
    private $container;


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('modGames', [$this, 'modGames']),
            new TwigFunction('mustCross', [$this, 'mustCross']),
            new TwigFunction('gameProgress', [$this, 'gameProgress']),
            new TwigFunction('allCount', [$this, 'allCount']),
            new TwigFunction('donePlayerProjects', [$this, 'donePlayerProjects']),
            new TwigFunction('canJoin', [$this, 'canJoin']),
        ];
    }

    /**
     * @param User|null $user
     * @param Game $game
     * @return mixed
     */
    public function canJoin($user, $game)
    {
        if (is_null($user)) return false;
        return $game->active() && !$game->inGame($user) && (is_null($game->getPlayerLimit()) || $game->getActivePlayers()->count() < $game->getPlayerLimit()) && ($game->getCanJoin() || ($game->getStart() > new Date()));
    }

    /**
     * @param PlayerStep|null $playerStep
     * @return mixed
     */
    public function allCount($playerStep)
    {
        if (is_null($playerStep)) return 0;
        return array_reduce(
            $playerStep->getGameStep()->getGame()->getSteps()->filter(function (GameStep $gameStep) use ($playerStep) {
                return $gameStep->getDate() <= $playerStep->getGameStep()->getDate();
            })->getValues(),
            function ($sum, GameStep $gameStep) use ($playerStep) {
                return $sum + array_reduce($gameStep->getSteps()->filter(function (PlayerStep $ps) use ($playerStep) {
                        return $ps->getPlayer() === $playerStep->getPlayer();
                    })->toArray(), function ($sum, PlayerStep $sps) {
                        return $sum + $sps->getCount();
                    }, 0);
            }, 0);
    }

    /**
     * @param PlayerStep|null $playerStep
     * @return mixed
     * @throws \Exception
     */
    public function donePlayerProjects($playerStep)
    {
        if (is_null($playerStep)) return [];
        $donePlayerProjects = (new ArrayCollection($this->container->get(GameService::class)
            ->getDoneProjects($playerStep->getGameStep()->getGame())))
            ->filter(function (Project $project) use ($playerStep) {
                return $project->getUser() === $playerStep->getPlayer()->getUser();
            });
        return $donePlayerProjects;
    }

    public function modGames()
    {
        return count($this->container->get(GameService::class)->getGames());
    }

    /**
     * @param Player $player
     * @return integer
     * @throws \Exception
     */
    public function mustCross(Player $player)
    {
        $game = $player->getGame();
        $penalty = $player->lastStep() ? $player->lastStep()->getPenalty() : 0;
        $res = $game->getCrossNorm() + $penalty - $this->gameProgress($player);
        return $res < 0 ? 0 : $res;
    }

    /**
     * @param Player $player
     * @return integer
     * @throws \Exception
     */
    public function gameProgress(Player $player)
    {
        $game = $player->getGame();
        $progress = $this->container->get(ProgressService::class)->getByPeriod(
            $game->lastStepDate(),
            new \DateTime(),
            $player->getProject());
        return array_reduce($progress, function ($sum, Progress $progress) {
            return $sum + $progress->getCount();
        }, 0);
    }
}
