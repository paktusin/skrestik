<?php

namespace App\Twig;

use App\Entity\Comment;
use App\Entity\Game;
use App\Entity\Message;
use App\Entity\Progress;
use App\Entity\Project;
use App\Entity\User;
use App\Service\GameService;
use App\Service\MessageService;
use App\Service\ProgressService;
use App\Service\ProjectService;
use App\Service\UserService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class UserExtension extends AbstractExtension
{
    private $container;


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('newMessages', [$this, 'newMessages']),
            new TwigFilter('speed', [$this,'speed']),
        ];
    }

    public function newMessages(?User $user)
    {
        if(!$user) return 0;
        return $this->container->get(MessageService::class)->getNewMessagesCount($user);
    }

    public function speed(?User $user)
    {
        if(!$user) return 0;
        return $this->container->get(ProgressService::class)->getSpeed($user);
    }
}
