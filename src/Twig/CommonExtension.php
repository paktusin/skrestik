<?php

namespace App\Twig;

use App\Entity\Comment;
use App\Entity\Game;
use App\Entity\Message;
use App\Entity\Project;
use App\Entity\User;
use App\Service\GameService;
use App\Service\MessageService;
use App\Service\ProgressService;
use App\Service\ProjectService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class CommonExtension extends AbstractExtension
{
    private $container;

    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('filter_name', [$this, 'doSomething']),
            new TwigFilter('gameType', [$this, 'gameType']),
            new TwigFilter('stepDay', [$this, 'stepDay']),
            new TwigFilter('gameStatus', [$this, 'gameStatus']),
        ];
    }

    public function gameStatus(Game $game)
    {
        return is_null($game->getModerated()) ? 'На модерации' : ($game->getModerated() ? 'Одобрено' : 'Отказано');
    }

    public function gameType($type)
    {
        $types = [
            1 => 'Норма в неделю',
            2 => 'Кто быстрее',
            3 => 'Кто больше',
        ];
        return $types[$type];
    }

    public function stepDay($dayNum)
    {
        $types = [
            1 => 'Понедельник',
            2 => 'Вторник',
            3 => 'Среда',
            4 => 'Четверг',
            5 => 'Пятница',
            6 => 'Суббота',
            7 => 'Воскресенье',
        ];
        return $types[$dayNum];
    }

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('canAddFriend', [$this, 'canAddFriend']),
            new TwigFunction('active', [$this, 'active']),
            new TwigFunction('inWatch', [$this, 'inWatch']),
            new TwigFunction('canGame', [$this, 'canGame']),
            new TwigFunction('inGame', [$this, 'inGame']),
            new TwigFunction('newComments', [$this, 'newComments'])
        ];
    }

    public function canAddFriend(User $user)
    {
        /** @var User $user */
        $me = $this->container->get('security.token_storage')->getToken()->getUser();
        return !$me->hasRequestTo($user) && !$me->hasFriend($user);
    }

    public function newComments(Project $project)
    {
        return $this->container
            ->get('doctrine')
            ->getRepository(Comment::class)
            ->count(['project' => $project, 'readed' => false]);
    }

    public function inGame(User $user, Game $game)
    {
        return $game->inGame($user);
    }

    public function active($path)
    {
        return $this->container->get('request_stack')->getCurrentRequest()->getPathInfo() === $path ? 'active' : '';
    }

    public function inWatch(Project $project)
    {
        /** @var User $user */
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        return $user->getWatchProjects()->contains($project);
    }

    public function canGame(?User $user)
    {
        return $this->container->get(GameService::class)->canGame($user);
    }
}
