<?php

namespace App\Form;

use App\Entity\Game;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GameType extends AbstractType
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('short')
            ->add('description', null, [
                'attr' => [
                    'data-emo' => true
                ]
            ])
            ->add('start', DateType::class, [
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'datepicker'
                ]
            ])
            ->add('end', DateType::class, [
                'required' => false,
                'label' => 'game_end',
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'datepicker'
                ]
            ])
            ->add('crossLimit')
            ->add('penaltyLimit')
            ->add('playerLimit')
            ->add('crossNorm')
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'Норма в неделю' => 1,
                    'Кто быстрее' => 2,
                    'Кто больше' => 3,
                ]
            ])
            ->add('stepDay', ChoiceType::class, [
                'choices' => [
                    'Понедельник' => 1,
                    'Вторник' => 2,
                    'Среда' => 3,
                    'Четверг' => 4,
                    'Пятница' => 5,
                    'Суббота' => 6,
                    'Воскресенье' => 7,
                ]
            ])
            ->add('canChange')
            ->add('canJoin')
            ->add('needPhoto');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Game::class,
        ]);
    }
}
