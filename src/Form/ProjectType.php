<?php

namespace App\Form;

use App\Entity\Author;
use App\Entity\Base;
use App\Entity\Project;
use App\Entity\Thread;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'attr' => [
                    'minlength' => 3,
                    'maxlength' => 50,
                ]
            ])
            ->add('crossX')
            ->add('crossY')
            ->add('crossCount')
            ->add('colorCount')
            ->add('baseColor', null, [
                'attr' => [
                    'minlength' => 3,
                    'maxlength' => 30,
                ]
            ])
            ->add('startDoneCross')
            ->add('endPlan', DateType::class, [
                'widget' => 'single_text',
//                'format' => 'dd.MM.yyyy',
                'required' => false,
                'attr' => [
                    'class' => 'datepicker'
                ]
            ])
            ->add('start', DateType::class, [
                'widget' => 'single_text',
                'required' => false,
                'attr' => [
                    'class' => 'datepicker'
                ]
            ])
            ->add('end', DateType::class, [
                'widget' => 'single_text',
//                'format' => 'dd.MM.yyyy',
                'required' => false,
                'attr' => [
                    'class' => 'datepicker'
                ]
            ])
            ->add('author', EntityType::class, [
                'class' => Author::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('a')
                        ->orderBy('a.name', 'ASC');
                },
                'group_by' => function (Author $choice) {
                    return $choice->getType();
                }
            ])
            ->add('thread', EntityType::class, [
                'class' => Thread::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('a')
                        ->orderBy('a.name', 'ASC');
                },
                'group_by' => function (Thread $choice) {
                    return $choice->getType();
                }
            ])
            ->add('base', EntityType::class, [
                'class' => Base::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('a')
                        ->orderBy('a.name', 'ASC');
                },
                'group_by' => function (Base $choice) {
                    return $choice->getType();
                }
            ])
            ->add('comment',null,[
                'attr'=>[
                    'data-emo'=>true
                ]
            ])
            ->add('hidden')
            ->add('disableComments')
            ->add('image_file', FileType::class, [
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Выберите файл',
                    'accept' => 'image/*'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Project::class,
        ]);
    }
}
