<?php
/**
 * Created by PhpStorm.
 * User: paktus
 * Date: 21.10.18
 * Time: 19:23
 */

namespace App\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class RewardType extends AbstractType
{

    public function getParent()
    {
        return ChoiceType::class;
    }

    public function getBlockPrefix()
    {
        return 'reward_type';
    }
}