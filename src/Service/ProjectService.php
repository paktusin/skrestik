<?php
/**
 * Created by PhpStorm.
 * User: paktus
 * Date: 21.09.18
 * Time: 8:12
 */

namespace App\Service;

use App\Entity\Player;
use App\Entity\Project;
use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;

class ProjectService extends Service
{

    protected $class = Project::class;

    /**
     * @param User $user
     * @param bool $noActivePlayer
     * @return Project[]
     */
    public function getCurrent(User $user, $noActivePlayer = false)
    {
        $q = $this->query('p')
            ->innerJoin('p.user', 'user')
            ->andWhere("user.id = {$user->getId()}")
            ->andWhere('p.end is null')
            ->andWhere('p.start is not null')
            ->orderBy('p.start', 'ASC');
        $res = $q->getQuery()
            ->getResult();
        return (new ArrayCollection($res))->filter(function (Project $project) use ($noActivePlayer) {
            return !$noActivePlayer || $project->getActivePlayers()->count() == 0;
        })->toArray();
    }

    /**
     * @param User $user
     * @return Project[]
     */
    public function getFuture(User $user)
    {
        $q = $this->query('p')
            ->innerJoin('p.user', 'user')
            ->andWhere("user.id = {$user->getId()}")
            ->andWhere('p.start is null')
            ->andWhere('p.end is null')
            ->orderBy('p.name', 'ASC');
        if (!$this->itsMe($user)) {
            $q->andWhere('p.hidden = 0');
        }
        return $q->getQuery()
            ->getResult();
    }

    /**
     * @param User $user
     * @return Project[]
     */
    public function getDone(User $user)
    {
        $q = $this->query('p')
            ->innerJoin('p.user', 'user')
            ->andWhere("user.id = {$user->getId()}")
            ->andWhere('p.end is not null')
            ->orderBy('p.end', 'DESC');
        if (!$this->itsMe($user)) {
            $q->andWhere('p.hidden = 0');
        }
        return $q->getQuery()
            ->getResult();
    }

    /**
     * @param User $user
     * @return bool
     */
    private function itsMe(User $user)
    {
        $currentUser = $this->container->get('security.token_storage')->getToken()->getUser();
        return $currentUser instanceof User && $user === $currentUser;
    }
}