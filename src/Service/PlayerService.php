<?php
/**
 * Created by PhpStorm.
 * User: paktus
 * Date: 21.09.18
 * Time: 8:12
 */

namespace App\Service;

use App\Entity\Comment;
use App\Entity\Game;
use App\Entity\Message;
use App\Entity\Player;
use App\Entity\Project;
use App\Entity\User;

class PlayerService extends Service
{
    protected $class = Player::class;

}