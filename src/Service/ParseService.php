<?php
/**
 * Created by PhpStorm.
 * User: paktus
 * Date: 21.09.18
 * Time: 8:12
 */

namespace App\Service;

use App\Entity\Author;
use App\Entity\Comment;
use App\Entity\Progress;
use App\Entity\Project;
use App\Entity\Thread;
use App\Entity\User;
use Sunra\PhpSimple\HtmlDomParser;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ParseService extends Service
{

    private $url = 'http://skolko-krestikov.ru';

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
    }


//    public function parseUsers()
//    {
//        for ($i = 1; $i <= 185; $i++) {
//            $page = ($i == 1) ? '' : $i . '/';
//            $users_dom = HtmlDomParser::str_get_html(file_get_contents($this->url . '/users/' . $page));
//            $trs = $users_dom->find('.proekt_list tr');
//            foreach ($trs as $key => $tr) {
//                if ($key === 0 || $key === count($trs) - 1) continue;
//                $user_href = $tr->find('td')[1]->find('a')[0];
//                $user_id = intval(str_replace('profile/', '', $user_href->attr['href']));
//                $this->getOrParseUser($user_id);
//            }
//        }
//        return true;
//    }

    public function getOrParseUser($user_id)
    {
        $user = $this->container->get('doctrine')->getRepository(User::class)->findOneBy(['parse_id' => $user_id]);
        if (!$user) {
            $content = iconv('windows-1251', 'utf-8', file_get_contents($this->url . '/profile/' . $user_id . '/'));
            $profile_dom = HtmlDomParser::str_get_html($content);
            $user = new User();
            $user->setParseId($user_id);
            $user->setPlainPassword(uniqid());
            $user->setEnabled(false);
            $info_p = $profile_dom->find('#right_side_in p')[0]->innertext();
            $user->setName($this->getInfo('Имя: ', $info_p));
            $user_created_at = date_create_from_format('d.m.Y', $this->getInfo('Дата регистрации: ', $info_p));
            $user->setCreatedAt($user_created_at ? $user_created_at : new \DateTime());
            $user->setCity($this->getInfo('Город: ', $info_p));
            if (count($profile_dom->find('#right_side_in p a')) > 0) {
                $user->setSocial($profile_dom->find('#right_side_in p a')[0]->attr['href']);
            }
            $username = trim($profile_dom->find('#right_side_in a.under')[0]->text());
            $user->setUsername($username);
            $user->setEmail($username . '@lost.mail');
            $avatar_url = $profile_dom->find('#avatar img')[0]->attr['src'];
            if (mb_strpos($avatar_url, 'img/avatarb.gif') === false)
                $user->setImage($this->container->get(ImageService::class)->uploadFromUrl($this->url . '/' . $avatar_url));
            $this->em->persist($user);
            $this->em->flush();
        }
        return $user;
    }

    public function getOrParseProject($project_id, User $user)
    {
        $project = $this->container->get(ProjectService::class)->repo()->findOneBy(['parse_id' => $project_id]);
        if (!$project) {
            $project_dom = HtmlDomParser::str_get_html(iconv('windows-1251', 'utf-8', file_get_contents($this->url . '/proekt/' . $project_id . '/')));
            if (!$project_dom) return null;
            $project_info = $project_dom->find('#right_side_in')[0]->innertext();
            $project = new Project();
            $project->setUser($user);
            $project->setParseId($project_id);
            $project->setName(trim($project_dom->find('#left_contant h1')[0]->text()));
            $project->setAuthor($this->container->get('doctrine')->getRepository(Author::class)->findOneBy([
                'name' => $this->getInfo('Дизайн: ', $project_info)
            ]));
            $project->setThread(
                $this->container->get('doctrine')->getRepository(Thread::class)->findOneBy([
                    'name' => $this->getInfo('Нитки: ', $project_info)
                ])
            );
            $project->setStartDoneCross(intval($this->getInfo('До начала проекта было вышито: ', $project_info)));
            if ($this->getInfo('Планируется вышить к дате: ', $project_info))
                $project->setEndPlan(date_create_from_format('d.m.Y', $this->getInfo('Планируется вышить к дате: ', $project_info)));
            if (strpos($project_info, 'Размер вышивки: неизвестен.') === false) {
                $size = $this->getInfo('Размер вышивки: ', $project_info);
                if ($size) {
                    $size_arr = explode('х', $size);
                    $project->setCrossX(intval($size_arr[0]));
                    $project->setCrossY(intval($size_arr[1]));
                }
            }
            $project->setCrossCount(intval(str_replace(' ', '', $this->getInfo('Общее количество крестиков: ', $project_info))));
            $image_url = $project_dom->find('#picture_pr .show_big_pic');
            if (count($image_url) > 0) {
                $image_url = $image_url[0]->attr['href'];
                $project->setImage($this->container->get(ImageService::class)->uploadFromUrl($this->url . '/' . $image_url));
            }
            $all_progress_tr = $project_dom->find('.proekt_list tr.c');
            $photos_dom = HtmlDomParser::str_get_html(iconv('windows-1251', 'utf-8', file_get_contents($this->url . '/proekt/' . $project_id . '/foto/')));
            foreach ($photos_dom->find('#otsh div') as $photo_dom) {
                $image = $this->container->get(ImageService::class)->uploadFromUrl($this->url . '/' . $photo_dom->find('.show_big_pic')[0]->attr['href']);
                $date_string = $photo_dom->text();
                $image->setCreatedAt(date_create_from_format('d.m.Y', $date_string));
                $project->addImage($image);
            }
            $this->em->persist($project);
            $this->em->flush();
            foreach ($photos_dom->find('#komment_box p') as $comment_p) {
                $user_id = intval(str_replace('profile/', '', $comment_p->find('.user')[0]->attr['href']));
                $comment_user = $this->getOrParseUser($user_id);
                if ($comment_user) {
                    $comment = new Comment();
                    $comment->setUser($comment_user);
                    $comment->setProject($project);
                    $comment->setCreatedAt(date_create_from_format('d.m.Y H:i', trim($comment_p->find('b span')[0]->text())));
                    $comment->setComment($comment_p->find('.komment_content')[0]->text());
                    $this->em->persist($comment);
                    $this->em->flush();
                }
            }
            foreach ($all_progress_tr as $progress_tr) {
                $tds = $progress_tr->find('td');
                if (count($tds) === 4) {
                    $progress = new Progress();
                    $progress->setProject($project);
                    $progress->setDate(date_create_from_format('d.m.Y', $tds[0]->text()));
                    $progress->setCount($tds[1]->find('strong')[0]->text());
                    $this->em->persist($progress);
                    $this->em->flush();
                }
            }
            if ($this->getInfo('Начало вышивки: ', $project_info))
                $project->setStart(date_create_from_format('d.m.Y', $this->getInfo('Начало вышивки: ', $project_info)));
            if (count($project_dom->find('.date')) > 1) {
                $project->setStart($this->parseDate($project_dom->find('.date')[0]->text()));
                $project->setEnd($this->parseDate($project_dom->find('.date')[1]->text()));
            }
            if (count($project_dom->find('#right_side p.blue')) > 0) {
                $project->setComment($project_dom->find('#right_side p.blue')[0]->text());
            }
        }
        return $project;
    }

    public function parseProjects()
    {
        for ($i = 617; $i <= 1157; $i++) {
            echo("парсим страницу {$i}  \n");
            $page = $i == 1 ? '' : $i . '/';
            $all_projects_dom = HtmlDomParser::str_get_html(file_get_contents($this->url . '/all_proekt/' . $page));
            foreach ($all_projects_dom->find('.proekt_list div') as $project_div) {
                $project_id = intval(str_replace('proekt/', '', $project_div->find('a.b')[0]->attr['href']));
                $user_id = intval(str_replace('profile/', '', $project_div->find('a.user')[0]->attr['href']));
                $this->getOrParseProject($project_id, $this->getOrParseUser($user_id));
            }
        }
        return true;
    }

    private function getInfo($name, $content, $tag = 'strong')
    {
        $search_index = strpos($content, $name);
        if ($search_index === false || strpos($content, $name . ' неизвестно.') !== false) return null;
        $content = substr($content, strpos($content, $name . "<{$tag}>") + strlen($name . "<{$tag}>"));
        $content = substr($content, 0, strpos($content, "</{$tag}>"));
        return $content;
    }

    /**
     * @param $str_date
     * @return \DateTime|null
     */
    public function parseDate($str_date)
    {
        $ru_month = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
        $en_month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        $str_date = str_replace($ru_month, $en_month, trim($str_date));
        $date = date_create_from_format('d F', $str_date);
        if (!$date)
            $date = date_create_from_format('d F y', $str_date);
        if (!$date) return null;
        return $date;
    }
}