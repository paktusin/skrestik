<?php
/**
 * Created by PhpStorm.
 * User: paktus
 * Date: 21.09.18
 * Time: 8:12
 */

namespace App\Service;

use App\Entity\Game;
use App\Entity\GameStep;
use App\Entity\PlayerStep;
use Doctrine\ORM\QueryBuilder;

class PlayerStepService extends Service
{

    protected $class = PlayerStep::class;

}