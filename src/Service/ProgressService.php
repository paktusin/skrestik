<?php
/**
 * Created by PhpStorm.
 * User: paktus
 * Date: 21.09.18
 * Time: 8:12
 */

namespace App\Service;

use App\Entity\Progress;
use App\Entity\Project;
use App\Entity\User;
use Symfony\Component\Validator\Constraints\Date;

class ProgressService extends Service
{
    protected $class = Progress::class;

    /**
     * @param User $user
     * @return float
     */
    public function getSpeed(User $user)
    {
        $end = (new \DateTime())->add(\DateInterval::createFromDateString('1 day'))->setTime(0, 0, 0);
        $start = (clone $end)->add(\DateInterval::createFromDateString('-3 month'));
        /** @var Progress[] $progresses */
        $progresses = $this->query('progress')
            ->join('progress.project', 'project')
            ->join('project.user', 'user')
            ->where("user.id = {$user->getId()}")
            ->andWhere("progress.date > :start")
            ->andWhere("progress.date < :end")
            ->orderBy('progress.date', 'asc')
            ->setParameter('end', $end)
            ->setParameter('start', $start)
            ->getQuery()
            ->getResult();
//        /** @var Project[] $projects */
//        $projects = $this->container->get(ProjectService::class)
//            ->query('project')
//            ->join('project.user', 'user')
//            ->where("user.id = {$user->getId()}")
//            ->andWhere('project.startDoneCross > 0')
//            ->andWhere('project.start is not null')
//            ->andWhere('project.start > :start')
//            ->andWhere('project.start < :end')
//            ->setParameter('end', $end)
//            ->setParameter('start', $start)
//            ->getQuery()->getResult();

        $dateStart = new \DateTime();
        if (count($progresses) > 0) {
            $dateStart = $progresses[0]->getDate();
        }
//        if (count($projects) > 0 && $projects[0]->getStart() < $dateStart) {
//            $dateStart = $projects[0]->getStart();
//        }
        $days = $end->diff($dateStart)->days;
        $progress_val = array_reduce($progresses, function ($sum, Progress $progress) {
            return $sum + $progress->getCount();
        }, 0);
//        $projects_val = array_reduce($projects, function ($sum, Project $progress) {
//            return $sum + intval($progress->getStartDoneCross());
//        }, 0);
        return round($progress_val / ($days <= 0 ? 1 : $days), 0);
    }

    /**
     * @param \DateTime $start
     * @param \DateTime $end
     * @param Project $project
     * @return Progress[]
     */
    public function getByPeriod(\DateTime $start, \DateTime $end, Project $project)
    {
        return $this->query('p')
            ->innerJoin('p.project','project')
            ->andWhere("project.id = {$project->getId()}")
            ->andWhere('p.date >= :start')
            ->andWhere('p.date < :end')
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->getQuery()
            ->getResult();
    }
}