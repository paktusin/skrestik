<?php
/**
 * Created by PhpStorm.
 * User: paktus
 * Date: 21.09.18
 * Time: 8:12
 */

namespace App\Service;

use App\Entity\Image;
use Gumlet\ImageResize;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CommonService extends Service
{
    /**
     * @param \DateTime $date
     * @param $day
     * @param bool $can_today
     * @return \DateTime|false
     */
    public function nearDay(\DateTime $date, $day, $can_today = true)
    {
        if ($can_today && intval($date->format('N')) === $day) return $date;
        $days = [
            '',
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
            'Sunday'
        ];
        /** @var \DateTime $nextDate */
        $nextDate = (new \DateTime())->setTimestamp(strtotime("next {$days[$day]}", $date->getTimestamp()));
        return $nextDate->setTime(23, 59, 59);
    }
}