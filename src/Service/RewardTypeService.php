<?php
/**
 * Created by PhpStorm.
 * User: paktus
 * Date: 21.09.18
 * Time: 8:12
 */

namespace App\Service;


use App\Entity\RewardType;

class RewardTypeService extends Service
{

    protected $class = RewardType::class;

    public function getActive()
    {
        return $this->repo->findBy(['enabled' => true]);
    }
}