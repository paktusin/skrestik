<?php
/**
 * Created by PhpStorm.
 * User: paktus
 * Date: 21.09.18
 * Time: 8:12
 */

namespace App\Service;

use App\Entity\Message;
use App\Entity\Project;
use App\Entity\User;
use Doctrine\ORM\Query;

class MessageService extends Service
{
    protected $class = Message::class;

    private function dialogQuery(User $user1, User $user2)
    {
        return $this->query('m')
            ->leftJoin('m.to', 'toUser')
            ->leftJoin('m.from', 'fromUser')
            ->where('toUser.id = :user1_id and fromUser.id = :user2_id')
            ->orWhere('toUser.id = :user2_id and fromUser.id = :user1_id')
            ->setParameter('user1_id', $user1->getId())
            ->setParameter('user2_id', $user2->getId());
    }

    /**
     * @param User $user1
     * @param User $user2
     * @return Query
     */
    public function getDialog(User $user1, User $user2)
    {
        return $this->dialogQuery($user1, $user2)
            ->orderBy('m.created_at', 'DESC')
            ->getQuery();
    }

    public function getDialogLastMessage(User $user1, User $user2)
    {
        $query = $this->dialogQuery($user1, $user2)
            ->orderBy('m.created_at', 'DESC')
            ->setMaxResults(1)
            ->getQuery()->getResult();
        return count($query) > 0 ? $query[0] : null;
    }

    public function mailMessage(User $from, User $to)
    {
        return $this->mailUser($from, $to, 'У вас новое сообщение',
            $this->container->get('twig')->render('/mail/message.html.twig', [
                'from' => $from,
                'to' => $to
            ]));
    }

    public function mailComment(User $from, Project $project)
    {
        return $this->mailUser($from, $project->getUser(), 'У вашего проекта новый комментарий',
            $this->container->get('twig')->render('/mail/comment.html.twig', [
                'project' => $project,
                'from' => $from,
                'to' => $project->getUser()
            ]));
    }

    public function mailUser(User $from, User $to, $subject, $body)
    {
        if (!$to->isEnabled() || !$to->getNotify()) return;
        $message = new \Swift_Message($subject);
        $message->setTo($to->getEmail());
        $message->setFrom($this->container->getParameter('fos_user.resetting.email.from_email'));
        $message->setBody($body, 'text/html');
        $this->container->get('mailer')->send($message);
    }

    public function getNewMessagesCount(User $user, ?User $from = null)
    {

        $res = $this->query('m')
            ->select('count(m)')
            ->join('m.to', 'user')
            ->join('m.from', 'fromUser')
            ->where("user.id = {$user->getId()}")
            ->andWhere('m.readed = 0');
        if ($from) {
            $res->andWhere("fromUser = {$from->getId()}");
        }
        return intval($res->getQuery()
            ->getSingleScalarResult());
    }
}