<?php
/**
 * Created by PhpStorm.
 * User: paktus
 * Date: 21.09.18
 * Time: 9:10
 */

namespace App\Service;


use Doctrine\ORM\QueryBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Service
{
    protected $class;
    protected $repo;
    protected $em;
    protected $container;

    /**
     * Service constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        if ($this->class) {
            $this->repo = $container->get('doctrine')->getRepository($this->class);
        }
        $this->em = $container->get('doctrine')->getManager();
    }

    public function repo()
    {
        return $this->repo;
    }

    /**
     * @param string $bind
     * @return QueryBuilder
     */
    public function query($bind = 'e')
    {
        return $this->repo->createQueryBuilder($bind);
    }

    public function find($id)
    {
        $object = $this->repo()->find($id);
        if (!$object) throw new NotFoundHttpException($this->container->get('translator')->trans($this->shortClass()).' не найден');
        return $object;
    }

    private function shortClass()
    {
        $index = strrpos($this->class, '\\');
        if ($index) {
            return substr($this->class, strrpos($this->class, '\\') + 1);
        } else {
            return $this->class;
        }
    }
}