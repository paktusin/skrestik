<?php
/**
 * Created by PhpStorm.
 * User: paktus
 * Date: 21.09.18
 * Time: 8:12
 */

namespace App\Service;

use App\Entity\User;

class UserService extends Service
{

    protected $class = User::class;

    public function dialogUsers(User $me)
    {
        $res = [];
        $users = $this->query('u')
            ->leftJoin('u.inMessages', 'inMessages')
            ->leftJoin('u.outMessages', 'outMessages')
            ->leftJoin('inMessages.from', 'fromUser')
            ->leftJoin('outMessages.to', 'toUser')
            ->where('fromUser.id = :user_id')
            ->orWhere('toUser.id = :user_id')
            ->setParameter('user_id', $me->getId())
            ->getQuery()
            ->getResult();
        foreach ($users as $user) {
            $res[] = [
                'user' => $user,
                'message' => $this->container->get(MessageService::class)->getDialogLastMessage($me, $user),
                'new' => $this->container->get(MessageService::class)->getNewMessagesCount($me, $user)
            ];
        }
        usort($res, function ($rowA, $rowB) {
            if ($rowA['message']->getReaded() === $rowB['message']->getReaded()) {
                return $rowA['message']->getCreatedAt() > $rowB['message']->getCreatedAt() ? -1 : 1;
            }
            return $rowA['message']->getReaded() ? 1 : -1;
        });
        return $res;
    }
}