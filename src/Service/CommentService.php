<?php
/**
 * Created by PhpStorm.
 * User: paktus
 * Date: 21.09.18
 * Time: 8:12
 */

namespace App\Service;

use App\Entity\Comment;
use App\Entity\Game;
use App\Entity\Project;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;

class CommentService extends Service
{
    protected $class = Comment::class;


    /**
     * @param $message string|null
     * @param User $user
     * @param Game|null $game
     * @param Project|null $project
     * @return Comment|null
     */
    public function addComment($message, User $user, $game = null, $project = null)
    {
        $commentText = trim($message . '');
        if (strlen($commentText) === 0) {
            return null;
        } else {
            $comment = new Comment();
            $comment->setGame($game);
            $comment->setProject($project);
            $comment->setUser($user);
            $comment->setReaded(true);
            $comment->setComment($commentText);
            $this->em->persist($comment);
            $this->em->flush();
            return $comment;
        }
    }

    /**
     * @param Request $request
     * @param Comment $comment
     * @param User $user
     * @param Game|null $game
     * @param Project|null $project
     * @return Comment|null
     */
    public function editComment(Request $request, Comment $comment, User $user, $game = null, $project = null)
    {
        $commentText = trim($request->request->get('comment'));
        if ($comment && $comment->getUser() === $user && strlen($commentText) === 0) {
            return null;
        } else {
            $comment->setGame($game);
            $comment->setProject($project);
            $comment->setComment($commentText);
            $this->em->flush();
            return $comment;
        }
    }
}