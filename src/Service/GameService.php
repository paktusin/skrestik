<?php
/**
 * Created by PhpStorm.
 * User: paktus
 * Date: 21.09.18
 * Time: 8:12
 */

namespace App\Service;

use App\Entity\Comment;
use App\Entity\Game;
use App\Entity\GameStep;
use App\Entity\Message;
use App\Entity\Player;
use App\Entity\PlayerStep;
use App\Entity\Progress;
use App\Entity\Project;
use App\Entity\User;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;

class GameService extends Service
{
    protected $class = Game::class;

    /**
     * @param User|null $user
     * @return bool
     * @throws \Exception
     */
    public function canGame(?User $user)
    {
        if (is_null($user)) return false;
        if (!$this->container->getParameter('check_gamer')) return true;
        $messages = intval($this->container->get('doctrine')->getManager()->createQueryBuilder()
            ->select('count(m.id)')
            ->from(Message::class, 'm')
            ->innerJoin('m.from', 'fromUser')
            ->andWhere("fromUser.id = {$user->getId()}")
            ->getQuery()->getSingleScalarResult());

        $comments = intval($this->container->get('doctrine')->getManager()->createQueryBuilder()
            ->select('count(c.id)')
            ->from(Comment::class, 'c')
            ->innerJoin('c.user', 'user')
            ->andWhere("user.id = {$user->getId()}")
            ->getQuery()->getSingleScalarResult());

        $doneCross = array_reduce($this->container->get(ProjectService::class)->getDone($user), function ($summ, Project $project) {
            return $summ + $project->getCrossCount();
        }, 0);
        return $user->getMyGames()->count() > 0 || $doneCross > 20000 && ($messages + $comments) >= 50 && (new DateTime())->diff($user->getCreatedAt())->m >= 3;
    }

    /**
     * @param bool|null $moderated
     * @param null $active
     * @return Game[]
     * @throws \Exception
     */
    public function getGames($moderated = null, $active = null)
    {
        $q = $this->query('g');
        if (is_null($moderated))
            $q->andWhere('g.moderated is null');
        else {
            $q->andWhere('g.moderated = :mod')
                ->setParameter('mod', $moderated);
        }
        if (!is_null($active)) {
            $now = new DateTime();
            if ($active) {
                $q->andWhere('g.end is null or g.end > :now');
            } else {
                $q->andWhere('g.end is not null and g.end <= :now');
            }
            $q->setParameter('now', $now);
        }
        return $q->getQuery()
            ->getResult();

    }

    public function makeStep(?DateTime $now = null)
    {
        if (!$now) $now = new \DateTime();
        $now->setTime(23, 59, 59);
        $activeGames = $this->getActive($now);
        foreach ($activeGames as $game) {

            $stepDate = $this->container->get(CommonService::class)->nearDay($now, $game->getStepDay());
            $hasSteps = $this->container
                ->get('doctrine')
                ->getRepository(GameStep::class)
                ->count(['game' => $game, 'date' => $stepDate]);
            if ($hasSteps === 0 && $stepDate === $now) {
                $gameStep = new GameStep();
                $gameStep->setDate($stepDate);
                $gameStep->setGame($game);
                $this->em->persist($gameStep);
                /** @var Player $player */
                foreach ($game->getActivePlayers() as $player) {
                    $playerStep = new PlayerStep();
                    $playerStep->setGameStep($gameStep);
                    $playerStep->setPlayer($player);
                    $playerStep->setProject($player->getProject());
                    $startDate = $game->lastStepDate();
                    $progress = new ArrayCollection();
                    $changes = $player->getLastProjectsInPeriod($startDate, $stepDate);
                    if (count($changes) > 0) {
                        foreach ($changes as $change) {
                            $changeDateStart = date_create_from_format('Y-m-d H:i:s', $change['date']);
                            $changeDateStart->setTime(0, 0, 0);
                            $changeDateEnd = date_create_from_format('Y-m-d H:i:s', $change['date']);
                            $changeDateEnd->setTime(23, 59, 59);
                            /** @var Project $lastProject */
                            $lastProject = $this->container->get(ProjectService::class)->repo()->find($change['id']);
                            if ($lastProject) {
                                foreach ($this->container->get(ProgressService::class)->getByPeriod($startDate, $changeDateEnd, $lastProject) as $p) {
                                    if (!$progress->contains($p)) $progress->add($p);
                                }
                            }
                            foreach ($this->container->get(ProgressService::class)->getByPeriod($changeDateStart, $stepDate, $player->getProject()) as $p) {
                                if (!$progress->contains($p)) $progress->add($p);
                            }
                        }
                    } else {
                        foreach ($this->container->get(ProgressService::class)->getByPeriod($startDate, $stepDate, $player->getProject()) as $p) {
                            if (!$progress->contains($p)) $progress->add($p);
                        }
                    }
                    $progressCount = $this->progressCount($progress->toArray());
                    $playerStep->setCount($progressCount);
                    if (!is_null($game->getCrossNorm())) {
                        $penalty = (!is_null($player->lastStep()) ? $player->lastStep()->getPenalty() : 0) + ($game->getCrossNorm() - $progressCount);
                        $playerStep->setPenalty($penalty < 0 ? 0 : $penalty);
                        if (!is_null($game->getPenaltyLimit())) {
                            $player->setGameOver($penalty > $game->getPenaltyLimit());
                        }
                    }
                    $gameStep->addStep($playerStep);
                }
                $this->em->flush();
            }
        }
    }

    /**
     * @param Progress[] $progress
     * @return int
     */
    private function progressCount($progress)
    {
        return array_reduce($progress, function ($summ, Progress $progress) {
            return $summ + $progress->getCount();
        }, 0);
    }

    public function quitGame(Game $game, Project $project, Player $player)
    {
        $steps = $this->container->get(PlayerStepService::class)->query('ps')
            ->join('ps.gameStep', 'gameStep')
            ->join('gameStep.game', 'game')
            ->join('ps.player', 'player')
            ->join('ps.project', 'project')
            ->andWhere("project.id = {$project->getId()}")
            ->andWhere("player.id = {$player->getId()}")
            ->andWhere("game.id = {$game->getId()}")
            ->getQuery()->getResult();;
        foreach ($steps as $step) {
            $this->em->remove($step);
        }
        $this->em->flush();
    }

    /**
     * @param \DateTime $date
     * @return Game[]
     */
    public function getActive(\DateTime $date)
    {
        return $this->query('game')
            ->andWhere('game.start < :date')
            ->andWhere('game.moderated = 1')
            ->andWhere('game.end is null or game.end > :date')
            ->setParameter('date', $date)
            ->getQuery()->getResult();
    }

    /**
     * @param Game $game
     * @return Project[]
     * @throws \Exception
     */
    public function getDoneProjects(Game $game)
    {
        $projects = new ArrayCollection();
        /** @var Player $player */
        foreach ($game->getPlayers() as $player) {
            foreach ($player->getLastProjects() as $change) {
                /** @var Project $changeProject */
                $changeProject = $this->container->get(ProjectService::class)->repo()->find($change['id']);
                if ($changeProject && !$projects->contains($changeProject) && $changeProject->isDone() && isset($change['done']) && $change['done'] === true) $projects->add($changeProject);
            }
        }
        return $projects->toArray();
    }
}