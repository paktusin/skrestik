<?php
/**
 * Created by PhpStorm.
 * User: paktus
 * Date: 21.09.18
 * Time: 8:12
 */

namespace App\Service;

use App\Entity\Game;
use App\Entity\GameStep;
use Doctrine\ORM\QueryBuilder;

class StepService extends Service
{

    protected $class = GameStep::class;


    /**
     * @param Game $game
     * @return GameStep[]
     */
    public function getLastSteps(Game $game)
    {
        $dates = $this->getDates($game);
        if (count($dates) === 0) return [];
        return $this->getSteps($game, $dates[0]);
    }

    /**
     * @param Game $game
     * @param \DateTime $date
     * @return GameStep[]
     */
    public function getSteps(Game $game, \DateTime $date)
    {
        return $this->query('gs')
            ->join('gs.player', 'player')
            ->join('player.user', 'user')
            ->join('player.game', 'game')
            ->where("game.id = {$game->getId()}")
            ->andWhere('gs.date = :date')
            ->setParameter('date', $date)
            ->orderBy('user.username', 'ASC')
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * @param Game $game
     * @return \DateTime[]
     */
    public function getDates(Game $game)
    {
        /** @var QueryBuilder $q */
        $q = $this->container->get('doctrine')->getManager()->createQueryBuilder();
        $dates = $q->select('gs.date')
            ->distinct(true)
            ->from(GameStep::class, 'gs')
            ->join('gs.player', 'player')
            ->join('player.game', 'game')
            ->where("game.id = {$game->getId()}")
            ->orderBy('gs.date', 'DESC')
            ->getQuery()
            ->getArrayResult();
        $res = array_reduce($dates, function ($arr, $row) {
            if (isset($row['date']) && $row['date'] instanceof \DateTime) {
                $arr[] = $row['date'];
            }
            return $arr;
        }, []);

        return $res;
    }

}