<?php
/**
 * Created by PhpStorm.
 * User: paktus
 * Date: 21.09.18
 * Time: 8:12
 */

namespace App\Service;

use App\Entity\Page;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PageService extends Service
{
    protected $class = Page::class;

    public function find($url)
    {
        $page = $this->repo()->findOneBy(['url' => $url]);
        if (!$page) throw new NotFoundHttpException('Страница не найдена');
        return $page;
    }
}