<?php
/**
 * Created by PhpStorm.
 * User: paktus
 * Date: 21.09.18
 * Time: 8:12
 */

namespace App\Service;

use App\Entity\Image;
use Gumlet\ImageResize;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageService extends Service
{

    protected $class = Image::class;
    private $path;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->path = $this->container->get('kernel')->getRootDir() . '/../public/upload/';
    }


    /**
     * @param null|UploadedFile $file
     * @param int $width
     * @param int $height
     * @param bool $crop
     * @param bool $mark
     * @return Image|ImageResize|null
     * @throws \Gumlet\ImageResizeException
     */
    public function upload(?UploadedFile $file, $width = 800, $height = 600, $crop = false, $mark = true)
    {
        if (!$file) return null;
        $filename = $this->randomName($file->getClientOriginalName());
        $file->move($this->path, $filename);
        $image_data = new ImageResize($this->path . $filename);
        $image_data->quality_jpg = 100;
        $image_data->quality_png = 0;
        if ($crop) {
            $image_data->crop($width, $height, true, ImageResize::CROPCENTER);
        } else {
            $image_data->resizeToBestFit($width, $height);
        }
        $image_data->save($this->path . $filename);
//        if ($thumb) {
//            $image_data->resizeToBestFit(250, 250);
//            $image_data->save($this->path . 'thumb_' . $filename);
//        }
        if ($mark) {
            $this->addText($this->path . $filename);
        }
        $image = new Image();
        $image->setName($filename);
        $this->em->persist($image);
        $this->em->flush();
        return $image;
    }

    private function addText($path)
    {
        if (!file_exists($path)) return null;
        $isPng = pathinfo($path, PATHINFO_EXTENSION) === 'png';
        $image = $isPng ? imagecreatefrompng($path) : imagecreatefromjpeg($path);
        $size = getimagesize($path);
        $white = imagecolorallocate($image, 230, 230, 230);
        $black = imagecolorallocate($image, 100, 100, 100);
        $font_path = $this->container->get('kernel')->getRootDir() . '/../assets/fonts/CeraPro-Regular.ttf';
        $text = '© ' . $this->container->get('security.token_storage')->getToken()->getUser()->getUserName() . '/zapiskihomyaka.ru';
        $this->imagettfstroketext($image, 14, 0, 9, 25, $white, $black, $font_path, $text, 1);
        $isPng ? imagepng($image, $path) : imagejpeg($image, $path);
        imagedestroy($image);
    }

    private function imagettfstroketext(&$image, $size, $angle, $x, $y, &$textcolor, &$strokecolor, $fontfile, $text, $px)
    {
        for ($c1 = ($x - abs($px)); $c1 <= ($x + abs($px)); $c1++)
            for ($c2 = ($y - abs($px)); $c2 <= ($y + abs($px)); $c2++)
                $bg = imagettftext($image, $size, $angle, $c1, $c2, $strokecolor, $fontfile, $text);
        return imagettftext($image, $size, $angle, $x, $y, $textcolor, $fontfile, $text);
    }


    /**
     * @param $url
     * @return Image|ImageResize
     */
    public function uploadFromUrl($url)
    {
        if (strpos($url, '?') !== false) {
            $url = substr($url, 0, strpos($url, '?'));
        }
        $image = new Image();
        $filename = $this->randomName($url);
        file_put_contents($this->path . $filename, file_get_contents($url));

        $image->setName($filename);
        $this->em->persist($image);
        $this->em->flush();
        return $image;
    }

    private function randomName($path)
    {
        return uniqid(rand(), true) . '.' . pathinfo($path, PATHINFO_EXTENSION);
    }

    public function removeImage(?Image $image)
    {
        if (!$image) return;
        $filePath = $this->path . $image->getName();
        if (file_exists($filePath)) {
            unlink($filePath);
        }
        $this->em->remove($image);
        $this->em->flush();
    }
}