<?php

namespace App\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class GameAdmin extends Admin
{
    protected $datagridValues = array(
        '_sort_order' => 'ASC',
        '_sort_by' => 'order_num',
    );

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('organizer')
            ->add('start', DatePickerType::class, [
                'format' => 'dd.MM.yyyy',
            ])
            ->add('end', DatePickerType::class, [
                'format' => 'dd.MM.yyyy',
                'required' => false
            ])
            ->add('description', null, [
                'attr' => [
                    'data-emo' => true
                ]
            ])
            ->add('crossLimit')
            ->add('penaltyLimit')
            ->add('playerLimit')
            ->add('crossNorm')
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'Норма в неделю' => 1,
                    'Кто быстрее' => 2,
                    'Кто больше' => 3,
                ]
            ])
            ->add('stepDay', ChoiceType::class, [
                'choices' => [
                    'Понедельник' => 1,
                    'Вторник' => 2,
                    'Среда' => 3,
                    'Четверг' => 4,
                    'Пятница' => 5,
                    'Суббота' => 6,
                    'Воскресенье' => 7,
                ]
            ])
            ->add('canChange')
            ->add('canJoin')
            ->add('needPhoto')
            ->add('organizer');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('start')
            ->add('end');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name')
            ->add('start')
            ->add('end');
        parent::configureListFields($listMapper);
    }

}
