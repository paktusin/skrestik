<?php

namespace App\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\DatePickerType;

class RewardAdmin extends Admin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('created_at', DatePickerType::class, [
                'format' => 'dd.MM.yyyy',
            ])
            ->add('player')
            ->add('reward_type')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('created_at')
            ->add('player')
            ->add('reward_type')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('created_at')
            ->add('player')
            ->add('reward_type')
        ;
        parent::configureListFields($listMapper);
    }

}
