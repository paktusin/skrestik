<?php

namespace App\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class RewardTypeAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $choices = array_slice(scandir(__DIR__ . '/../../public/rewards'), 2);
        $formMapper
            ->add('name')
            ->add('description')
            ->add('enabled')
            ->add('image', ChoiceType::class, [
                'choices' => $choices,
                'attr' => ['class' => 'rew']
            ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('enabled');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name')
            ->add('enabled')
            ->add('image');
        parent::configureListFields($listMapper);
    }

}
