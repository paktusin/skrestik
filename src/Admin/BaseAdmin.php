<?php

namespace App\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\DatePickerType;

class BaseAdmin extends Admin
{
    protected $datagridValues = array(
        '_sort_order' => 'ASC',
        '_sort_by' => 'order_num',
    );

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('count')
            ->add('type')
            ->add('coef',null,['label'=>'base.count'])
            ->add('delim')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('count',null,['label'=>'base.count'])
            ->add('type')
            ->add('coef')
            ->add('delim')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('name')
            ->add('count',null,['label'=>'base.count'])
            ->add('type')
            ->add('coef')
            ->add('delim')
        ;
        parent::configureListFields($listMapper);
    }

}
