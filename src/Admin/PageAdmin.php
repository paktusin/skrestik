<?php

namespace App\Admin;

use App\Entity\Page;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PageAdmin extends Admin
{
    protected $datagridValues = array(
        '_sort_order' => 'ASC',
        '_sort_by' => 'order_num',
    );

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title')
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'Новость' => Page::NEWS,
                    'Страница' => Page::PAGE
                ]
            ])
            ->add('url')
            ->add('created_at', DatePickerType::class, [
                'format' => 'dd.MM.yyyy',
            ])
            ->add('text', null, [
                'attr' => ['class' => 'cke']
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('url')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('title')
            ->add('url');
        parent::configureListFields($listMapper);
    }

}
