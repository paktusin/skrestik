<?php

namespace App\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\DatePickerType;

class ProjectAdmin extends Admin
{
    protected $datagridValues = array(
        '_sort_order' => 'ASC',
        '_sort_by' => 'order_num',
    );

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('crossX')
            ->add('crossY')
            ->add('crossCount')
            ->add('colorCount')
            ->add('baseColor')
            ->add('startDoneCross')
            ->add('endPlan', DatePickerType::class, [
                'format' => 'dd.MM.yyyy',
            ])
            ->add('start', DatePickerType::class, [
                'format' => 'dd.MM.yyyy',
            ])
            ->add('end', DatePickerType::class, [
                'format' => 'dd.MM.yyyy',
            ])
            ->add('author')
            ->add('thread')
            ->add('base')
            ->add('comment')
            ->add('hidden')
            ->add('disableComments')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('endPlan')
            ->add('start')
            ->add('end')
            ->add('author')
            ->add('thread')
            ->add('base')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name')
            ->add('size')
            ->add('start', null, [
                'format' => 'd.m.Y H:i:s'
            ])
            ->add('end', null, [
                'format' => 'd.m.Y H:i:s'
            ])
            ->add('author')
            ->add('thread')
            ->add('base')
        ;
        parent::configureListFields($listMapper);
    }

}
