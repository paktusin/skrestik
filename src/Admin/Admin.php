<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\CallbackTransformer;

class Admin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
    );

    protected $maxPerPage = 64;


    protected $persistFilters = true;

    protected function configureListFields(ListMapper $list)
    {
        unset($this->listModes['mosaic']);
        $list->add('_action', null, array(
            'actions' => array(
                'edit' => array(),
                'delete' => array(),
            ),
        ));
    }


    /**
     * @param [] $fields
     * @param FormMapper $formMapper
     */
    protected function transSimpleArr($fields, $formMapper)
    {
        foreach ($fields as $field) {
            $formMapper->get($field)
                ->addModelTransformer(new CallbackTransformer(
                    function ($arr) {
                        return implode(',', ($arr ? $arr : []));
                    },
                    function ($text) {
                        return explode(',', $text);
                    }
                ));
        }
    }
}
