<?php

namespace App\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Sonata\UserBundle\Form\Type\SecurityRolesType;

class UserAdmin extends Admin
{
    protected $datagridValues = array(
        '_sort_order' => 'ASC',
        '_sort_by' => 'order_num',
    );

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('username')
            ->add('email')
            ->add('name')
            ->add('city')
            ->add('social')
            ->add('birthdate', DatePickerType::class, [
                'format' => 'd.M.y',
                'required'=>false
            ])
            ->add('notify')
            ->add('enabled')
            ->add('roles',SecurityRolesType::class,[
                'label' => false,
                'expanded' => true,
                'multiple' => true,
                'required' => false,
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('username')
            ->add('email')
            ->add('name')
            ->add('city')
            ->add('enabled')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('username')
            ->add('email')
            ->add('name')
            ->add('city')
            ->add('created_at', null, [
                    'format' => 'd.m.Y H:i:s'
                ]
            )
            ->add('enabled')
        ;
        parent::configureListFields($listMapper);
    }

}
